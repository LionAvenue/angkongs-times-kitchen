<?php
require_once('../database/database.php');


class categoryController
{
	public function getCategoryList ($type = null) {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;
		$category = $type ? $type : 'menu';

		if (isset($_POST['categoryType'])) {
			$category = $_POST['categoryType'];
		}

		$stmt = $conn->db()->prepare("SELECT * FROM `category` WHERE  category_type = ? AND  `is_delete` = '0' ");
    	$stmt->execute([$category]);
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function createCategory () {
		$conn = new database();
		$category_name = $_POST['category_name'];
		$category_type = $_POST['category_type'];

		$stmt = $conn->db()->prepare("INSERT INTO `category` (`category_name`, `category_type`) VALUES (?, ?)");
		$stmt->execute([$category_name, $category_type]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));	
	}

	public function updateCategory () {
		$conn = new database();
		$category_id = $_POST['category_id'];
		$category_name = $_POST['category_name'];
		$category_type = $_POST['category_type'];

		$stmt = $conn->db()->prepare("UPDATE `category` set `category_name` = ?, `category_type` = ? WHERE `category_id` = ?");
		$stmt->execute([$category_name, $category_type, $category_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function deleteCategory () {
		$conn = new database();
		$category_id = $_POST['category_id'];

		$stmt = $conn->db()->prepare("UPDATE `category` SET `is_delete` = '1' WHERE `category_id` = ?");
		$stmt->execute([$category_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}
}

 ?>