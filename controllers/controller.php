<?php 
require_once('menuController.php');
require_once('employeeController.php');
require_once('packageController.php');
require_once('categoryController.php');
require_once('feedbackController.php');
require_once('utilityController.php');
require_once('reservationController.php');
require_once('notificationController.php');

$menu = new menuController();
$employee = new employeeController();
$package = new packageController();
$category = new categoryController();
$feedback = new feedbackController();
$utility = new utilityController();
$reservation = new reservationController();
$notification = new notificationController();

if (isset($_POST['requestType']) || isset($_GET['requestType'])) {
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$request = $_POST['requestType'];
	} else {
		$request = $_GET['requestType'];
	}

	if ($request == 'employeeLogin') {
		echo $employee->login();
		return;
	}

	if ($request == 'createEmployee') {
		echo $employee->createEmployee();
		return;
	}

	if ($request == 'updateEmployee') {
		echo $employee->updateEmployee();
		return;
	}

	if ($request == 'deleteEmployee') {
		echo $employee->deleteEmployee();
		return;
	}

	if ($request == 'createMenu') {
		echo $menu->createMenu();
		return;
	}

	if ($request == 'uploadMenuPicture') {
		echo $menu->uploadMenuPicture();
		return;
	}

	if ($request == 'getMenuList') {
		echo $menu->getMenuList();
		return;
	}

	if ($request == 'getMenu') {
		echo $menu->getMenu();
		return;
	}

	if ($request == 'updateMenu') {
		echo $menu->updateMenu();
		return;
	}

	if ($request =='deleteMenu') {
		echo $menu->deleteMenu();
		return;
	}

	if ($request == 'createPackage') {
		echo $package->createPackage();
		return;
	}

	if ($request == 'getPackage') {
		echo $package->getPackage();
		return;
	}

	if ($request == 'updatePackage') {
		echo $package->updatePackage();
		return;
	}

	if ($request == 'deletePackage') {
		echo $package->deletePackage();
		return;
	}

	if ($request == 'deletePackageItem') {
		echo $package->deletePackageItem();
		return;
	}

	if ($request == 'createCategory') {
		echo $category->createCategory();
		return;
	}

	if ($request == 'updateCategory') {
		echo $category->updateCategory();
		return;
	}

	if ($request == 'deleteCategory') {
		echo $category->deleteCategory();
		return;
	}

	if ($request == 'getCategoryList') {
		echo $category->getCategoryList();
		return;
	}

	if ($request == 'getFeedbackList') {
		echo $feedback->getFeedbackList();
		return;
	}

	if ($request == 'getLimitFeedbackList') {
		echo $feedback->getLimitFeedbackList();
		return;
	}

	if ($request == 'createUtility') {
		echo $utility->createUtility();
		return;
	}

	if ($request == 'updateUtility') {
		echo $utility->updateUtility();
		return;
	}

	if ($request == 'deleteUtility') {
		echo $utility->deleteUtility();
		return;
	}

	if ($request == 'getOrderList') {
		echo $reservation->getOrderList();
		return;
	}

	if ($request == 'getOrderLineList') {
		echo $reservation->getOrderLineList();
		return;
	}

	if ($request == 'updateCustomerOrder') {
		echo $reservation->updateCustomerOrder();
		return;
	}

	if ($request == 'updateCustomerOrderStatus') {
		echo $reservation->updateCustomerOrderStatus();
		return;
	}

	if ($request == 'returnUtility') {
		echo $utility->returnUtility();
		return;
	}

	if ($request == 'getNotification') {
		echo $notification->getNotification();
		return;
	}

	if ($request == 'updateOrderStatus') {
		echo $notification->updateOrderStatus();
		return;
	}

	if ($request == 'employeeLogout') {
		echo $employee->employeeLogout();
		return;
	}

	if ($request == 'createOrder') {
		echo $reservation->createOrder();
		return;
	}

	if ($request == 'updateAvailability') {
		echo $menu->updateAvailability();
		return;
	}

	if ($request == 'updatePackageAvailability') {
		echo $package->updatePackageAvailability();
		return;
	}
	if ($request == 'updateUtilityAvailability') {
		echo $utility->updateUtilityAvailability();
		return;
	}

	if ($request == 'getLimitEventDates') {
		echo $reservation->getLimitEventDates();
		return;
	}


	echo json_encode(array('status' => 'error', 'message' => '404 Not Found'));
	// {status: error, message: 404 not found}
}

?>