<?php
require_once('../database/database.php');
require_once('../session/sessionController.php');
require_once('hashController.php');

class EmployeeController
{
	public function login () {
		$conn = new database();
		$hash = new hashController();

		$email = $_POST['email'];
		$password = $hash->encryptHash($_POST['password']);

    	$stmt = $conn->db()->prepare("SELECT * FROM employee WHERE `emp_email` = ? AND `emp_password` = ?");
    	$stmt->execute([$email, $password]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'User not found')); 
    	}

    	session_start();
    	$_SESSION['employee'] = $row;

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $row));
	}

	public function createEmployee () {
		$conn = new database();
		$hash = new hashController();

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$middlename = $_POST['middlename'];
		$position = $_POST['position'];
		$email = $_POST['email'];
		$mobilenumber = $_POST['mobilenumber'];
		$password = $_POST['password'];

		if(!empty($password)) {
			$password = $hash->encryptHash($password);	
		}

		$stmt = $conn->db()->prepare("INSERT INTO `employee` (`emp_fname`,`emp_lname`,`emp_mi`,`emp_pos`,`emp_email`,`emp_number`, `emp_password`) VALUES (?, ?, ?, ?, ?, ?, ?)");
    	$stmt->execute([$firstname, $lastname, $middlename, $position, $email, $mobilenumber, $password]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function getEmployeeList () {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;

		$stmt = $conn->db()->prepare("SELECT * FROM `employee` WHERE  `is_delete` = '0' ");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function employeeLogout () {
		$session = new sessionController(); 
		$session->destroy();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function updateEmployee () {
		$conn = new database();
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$middlename = $_POST['middlename'];
		$position = $_POST['position'];
		$email = $_POST['email'];
		$mobilenumber = $_POST['mobilenumber'];
		$employee_id = $_POST['employee_id'];

		$stmt = $conn->db()->prepare("UPDATE `Employee` set `emp_fname` = ?, `emp_lname` = ?, `emp_mi` = ?, `emp_email` = ?, `emp_number` = ?, `emp_pos` = ? WHERE emp_id = ?");

    	$stmt->execute([$firstname, $lastname, $middlename, $email, $mobilenumber, $position, $employee_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Employee Updated!'));
	}

	public function deleteEmployee () {
		$conn = new database();
		$emp_id = $_POST['emp_id'];

		$stmt = $conn->db()->prepare("UPDATE `Employee` SET `is_delete` = '1' WHERE `emp_id` = ?");

    	$stmt->execute([$emp_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Employee Deleted!'));
	}
}

 ?>