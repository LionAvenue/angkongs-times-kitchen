<?php 

class FeedbackController
{
public function getFeedbackList () {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `feedback`");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
	}

	public function createCategory () {
		$conn = new database();
		$message = $_POST['messsage'];
		$date = $_POST['date_send'];
		$customer_id = $_POST['customer_id'];

		$stmt = $conn->db()->prepare("INSERT INTO `feedback` (`feedback_message`, `date_send`, `cust_id`) VALUES (?, ?, ?)");
		$stmt->execute([$message, $date, $customer_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));	
	}

	public function getLimitFeedbackList () {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `feedback` ORDER BY `feedback_id` DESC LIMIT 6");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));	
	}
}


?>