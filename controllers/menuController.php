<?php 
require_once('../database/database.php');
require_once('packageController.php');

class MenuController
{
	public function createMenu () {
		$conn = new database();
		$name = $_POST['menuName'];
		$category = $_POST['category'];
		$description = $_POST['description'];
		$price_xsmall = $_POST['price_xsmall'];
		$price_small = $_POST['price_small'];
		$price_medium = $_POST['price_medium'];
		$price_large = $_POST['price_large'];
		$price = $_POST['price'];
		$menu_image = "";


		if (isset($_FILES['menuFile'])) {		
			$src = $_FILES['menuFile']['tmp_name'];
			$menuFilePath = "../img/menu_img/".$_FILES['menuFile']['name'];
			move_uploaded_file($src, $menuFilePath);
			$menu_image = $_FILES['menuFile']['name']; 
		}


		$stmt = $conn->db()->prepare(
			"INSERT INTO `food` (`food_name`, `food_desc`, `price`, `price_xsmall`, `price_small`, `price_medium`, `price_large`, `category`, `food_image`) 
			VALUES (?, ?, ?, ?, ?, ? ,? ,?, ?)"
			);
    	$stmt->execute([$name, $description, $price, $price_xsmall, $price_small, $price_medium, $price_large, $category,$menu_image]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function getMenuList () {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;

		$stmt = $conn->db()->prepare("SELECT * FROM `food` WHERE  `is_delete` = '0' ");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function getMenu ($foodId = null) {
		$conn = new database();

		$food_id = isset($_POST['id']) ? $_POST['id'] : $foodId;

    	$stmt = $conn->db()->prepare("SELECT * FROM food WHERE `food_id` = ?");
    	$stmt->execute([$food_id]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Menu not found'));
    	}

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $row));
	}

	public function updateMenu () {
		$conn = new database();
		$id = $_POST['food_id'];
		$name = $_POST['menuName'];
		$category = $_POST['category'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$price_xsmall = $_POST['price_xsmall'];
		$price_small = $_POST['price_small'];
		$price_medium = $_POST['price_medium'];
		$price_large = $_POST['price_large'];
		$image = $_POST['current_img'];

		if (isset($_FILES['menuFile'])) {
			$src = $_FILES['menuFile']['tmp_name'];
			$imagePath = "../img/menu_img/".$_FILES['menuFile']['name'];
			$image = $_FILES['menuFile']['name'];
			move_uploaded_file($src, $imagePath);
		}

		$stmt = $conn->db()->prepare("UPDATE `food` set `food_name` = ?, `food_desc` = ?, `price_xsmall` = ?, `price_small` = ?, `price_medium` = ?, `price_large` = ?,`category` = ?, `food_image` = ? WHERE food_id = ?");

    	$stmt->execute([$name, $description, $price_xsmall, $price_small, $price_medium, $price_large, $category, $image, $id]);

		return json_encode(array('status' => 'OK', 'message' => 'Menu Updated!'));
	}

	public function deleteMenu () {
		$conn = new database();
		$package_item = new packageController();
		$food_id = $_POST['food_id'];
		$item = $package_item->viewPackageItem($food_id); 

		if (count($item)) {
			return json_encode(array('status' => 'error', 'message' => 'Item is used in package!'));
		}

		$stmt = $conn->db()->prepare("UPDATE `food` SET `is_delete` = '1' WHERE `food_id` = ?");

    	$stmt->execute([$food_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Menu Updated!'));
	}

	public function updateAvailability() {
		$conn = new database();

		$food_id = $_POST['id'];
		$status = $_POST['status'];
		

		$stmt = $conn->db()->prepare("UPDATE `food` SET `is_available` = ? where `food_id` = ?");
		$stmt->execute([$status , $food_id]);

		return json_encode( array('status' => 'OK' , 'message' => 'success' )); 
	}
}

?>