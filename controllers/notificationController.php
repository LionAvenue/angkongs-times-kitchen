<?php 
require_once('../database/database.php');

class NotificationController
{
	public function getNotification () {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT DISTINCT `created_at` FROM `order_tbl` WHERE `notif_status` = 0");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));	
	}

	public function updateOrderStatus () {
		$conn = new database();
		$order_id = $_POST['order_id'];

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` SET `notif_status` = ? WHERE `order_id` = ?");
    	$stmt->execute([1, $order_id]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}
}

 ?>