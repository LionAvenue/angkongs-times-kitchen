<?php
require_once('../database/database.php');
require_once('menuController.php');

class PackageController
{
	public function createPackage () {
		$conn = new database();
		$name = $_POST['packageName'];
		$category = $_POST['category'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$limit = $_POST['limit'];
		$package_image = "";


		if (isset($_FILES['packageImage'])) {
			$src = $_FILES['packageImage']['tmp_name'];
			$package_image_path = "../img/package_img/".$_FILES['packageImage']['name'];
			$package_image = $_FILES['packageImage']['name'];
			move_uploaded_file($src, $package_image_path);
		}

		$stmt = $conn->db()->prepare("INSERT INTO `package` (`package_name`, `package_desc`, `price`, `category`, `package_image`, `limit`) VALUES (?, ?, ?, ?, ?, ?)");
		$stmt->execute([$name, $description, $price, $category, $package_image, $limit]);

		$this->createPackageMenu();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function createPackageMenu () {
		$conn = new database();
		$package_id = $this->getPackageLastInsertId();
		
		if (isset($_POST['packageItem'])) {
			$package_item = json_decode($_POST['packageItem'], true);

			foreach ($package_item as $row) {
				$stmt = $conn->db()->prepare("INSERT INTO `package_item` (`quantity`, `package_id`, `food_id`, `price_size`, `size`) VALUES (?, ?, ?, ?, ?)");
				$stmt->execute([$row['quantity'], $package_id, $row['food_id'], $row['price_size'], $row['size']]);
			}
		}

		return true;
	}

	public function getPackageList () {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;

		$stmt = $conn->db()->prepare("SELECT * FROM `package` WHERE  `is_delete` = '0'");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function getPackage () {
		$conn = new database();

		$package_id = $_POST['package_id'];

    	$stmt = $conn->db()->prepare("SELECT * FROM package WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);
    	$row = $stmt->fetch();

    	if (empty($row)) {
			return json_encode(array('status' => 'error', 'message' => 'Package not found'));
    	}
		
		$package_item = $this->getPackageItem();
    	return json_encode(array('status' => 'OK', 'message' => 'success', 'package' => $row, 'package_item' => $package_item));
	}

	public function getPackageItem () {
		$conn = new database();
		$menu = new menuController();
		$package_item = array();

		$package_id = $_POST['package_id'];

    	$stmt = $conn->db()->prepare("SELECT * FROM package_item WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);
    	$rows = $stmt->fetchAll();

    	if (empty($rows)) {
			return json_encode(array('status' => 'error', 'message' => 'Package Items not found'));
    	}
		
    	$package_item = $this->getItemInstance();

		return $package_item;
    }

    public function getItemInstance () {
    	$conn = new database();
    	$package_id = $_POST['package_id'];

    	$stmt = $conn->db()->prepare("SELECT * FROM package_item INNER JOIN `food` ON `package_item`.food_id = `food`.food_id WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);
    	$rows = $stmt->fetchAll();

    	return $rows;
    }

    public function viewPackageItem ($id) {
    	$conn = new database();
		$food_id = $id;

    	$stmt = $conn->db()->prepare("SELECT * FROM package_item WHERE `food_id` = ?");
    	$stmt->execute([$food_id]);
    	$rows = $stmt->fetchAll();

		return $rows;
    }

	public function updatePackage () {
		$conn = new database();
		$package_id = $_POST['package_id'];
		$name = $_POST['packageName'];
		$category = $_POST['category'];
		$price = $_POST['price'];
		$limit = $_POST['limit'];
		$description = $_POST['description'];
		$createPackageItems = isset($_POST['packageItemCreate']) ? json_decode($_POST['packageItemCreate'], true) : null;
		$deletePackageItems = isset($_POST['packageItemDelete']) ? json_decode($_POST['packageItemDelete'], true) : null;
		$package_image = $_POST['current_pkg_img'];

		if (isset($_FILES['packageFile'])) {
			$src = $_FILES['packageFile']['tmp_name'];
			$package_image_path = "../img/package_img/".$_FILES['packageFile']['name'];
			$package_image = $_FILES['packageFile']['name'];
			move_uploaded_file($src, $package_image_path);
		}

		if ($createPackageItems) {
			foreach ($createPackageItems as $row) {
				$stmt = $conn->db()->prepare("INSERT INTO `package_item` (`quantity`, `package_id`, `food_id`, `price_size`, `size`) VALUES (?, ?, ?, ?, ?)");
				$stmt->execute([$row['quantity'], $package_id, $row['food_id'], $row['price_size'], $row['size']]);
			}		
		}

		if ($deletePackageItems) {
			foreach ($deletePackageItems as $row) {
				$stmt = $conn->db()->prepare("DELETE FROM `package_item` WHERE `package_id` = ? AND `package_item_id` = ?");
    			$stmt->execute([$package_id, $row['package_item_id']]);
			}
		}

		$stmt = $conn->db()->prepare("UPDATE `package` set `package_name` = ?, `package_desc` = ?, `price` = ?, `category` = ?, `package_image` = ?, `limit` = ? WHERE package_id = ?");
		$stmt->execute([$name, $description, $price, $category, $package_image, $limit, $package_id]);

		$this->updatePackageMenu($package_id);
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function updatePackageMenu ($packageId) {
		$package_id = $packageId;
		$package_item = isset($_POST['packageItem']) ? json_decode($_POST['packageItem'], true) : null;

		if ($package_item) {
			foreach ($package_item as $row) {
				$conn = new database();
				$stmt = $conn->db()->prepare("UPDATE `package_item` set `quantity` = ? WHERE package_id = ? AND package_item_id = ?");
				$stmt->execute([$row['quantity'], $package_id, $row['package_item_id']]);
			}
		}

		return true;
	}

	public function deletePackage () {
		$conn = new database();
		$package_id = $_POST['package_id'];

		$stmt = $conn->db()->prepare("UPDATE `package` SET `is_delete` = '1' WHERE `package_id` = ?");
    	$stmt->execute([$package_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Package Deleted!'));
	}

	public function deletePackageItem () {
		$conn = new database();
		$package_id = $_POST['package_id'];
		$package_item_id = $_POST['package_item_id'];

		$stmt = $conn->db()->prepare("DELETE FROM `package_item` WHERE `package_id` = ? AND `package_item_id` = ?");

    	$stmt->execute([$package_id, $package_item_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Package Item Deleted!'));
	}

	public function getPackageLastInsertId () {
		$conn = new database();
		$stmt = $conn->db()->prepare("SELECT `package_id` FROM `package` ORDER BY package_id DESC LIMIT 1");
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['package_id'];		
	}

	public function updatePackageAvailability() {
		$conn = new database();

		$package_id = $_POST['id'];
		$status = $_POST['status'];
		

		$stmt = $conn->db()->prepare("UPDATE `package` SET `is_available` = ? where `package_id` = ?");
		$stmt->execute([$status , $package_id]);

		return json_encode( array('status' => 'OK' , 'message' => 'success' )); 
	}

}

 ?>