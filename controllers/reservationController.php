<?php 
require_once('../database/database.php');

class reservationController
{
	public function getOrderList () {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT DISTINCT `created_at` FROM `order_tbl`");
		$stmt->execute();
		$rows = $stmt->fetchAll();

		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
	}

	public function getOrderListByDate ($date) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` INNER JOIN `customer` ON `order_tbl`.cust_id = `customer`.cust_id WHERE `created_at` = ?");
		$stmt->execute([$date]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getOrderLineList () {
		$conn = new database();
		$order_id = $_POST['order_id'];
		$menu = array();

		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl` WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$row = $stmt->fetch();

		$services = $this->getOrderService($order_id);
		$payment = $this->getOrderPayment($order_id);

		if (isset($row['package_id'])) {
			$menu = $this->getOrderPackage($row['package_id']);
		} else {
			$menu = $this->getOrderLine($order_id);
		}

    	return json_encode(array('status' => 'OK', 'message' => 'success', 'order' => $row, 'service' => $services, 'menu' => $menu, 'payment' => $payment));
	}


	public function getOrderLine ($order_id) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `orderline_tbl` INNER JOIN `food` ON `orderline_tbl`.food_id = `food`.food_id WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetchAll();

    	return $rows;	
	}

	public function getOrderService ($order_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `services` INNER JOIN `utilities` ON `services`.utility_id = `utilities`.utility_id WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getOrderPackage ($package_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `package_item` INNER JOIN `food` ON `package_item`.food_id = `food`.food_id WHERE `package_id` = ?");
		$stmt->execute([$package_id]);
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getOrderPayment ($order_id) {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `payment_logs` WHERE `order_id` = ?");
		$stmt->execute([$order_id]);
		$rows = $stmt->fetch();

		return $rows;
	}

	public function updateCustomerOrder () {
		$conn = new database();
		$order_id = $_POST['order_id'];
		$order_status = $_POST['order_status'];

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` set `order_status` = ? WHERE order_id = ?");
		$stmt->execute([$order_status, $order_id]);

		if ($order_status != 'Pending') {
			$this->customerNotification();
		}

		$this->updateCustomerPayment();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function customerNotification () {
		$conn = new database();
		$order_id = $_POST['order_id'];
	
		$stmt = $conn->db()->prepare("UPDATE `order_tbl` set `cust_notif_status` = ? WHERE order_id = ?");
		$stmt->execute([1, $order_id]);

		return true;		
	}

	public function updateCustomerPayment () {
		$conn = new database();
		$order_id = $_POST['order_id'];
		$downpayment = $_POST['downpayment']; 
		$amount_paid = $_POST['amount_paid'];
		$rem_bal = $_POST['remaining_balance'];
		$payment_status = $_POST['payment_status'];

		$stmt = $conn->db()->prepare("UPDATE `payment_logs` set `amount_paid` = ?, `downpayment` = ?, `rem_bal` = ?, `payment_status` = ? WHERE order_id = ?");
		$stmt->execute([$amount_paid, $downpayment, $rem_bal, $payment_status, $order_id]);

		return true;		
	}

	public function getPaymentList () {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `payment_logs` INNER JOIN `customer` ON `payment_logs`.cust_id = `customer`.cust_id");
		$stmt->execute();
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function createOrder () {
		$this->userRegister();
		$conn = new database();
		$payment = $_POST['payment'];
		$cust_id = $this->getCustomerLastInsertId();
		$order = $_POST['order'];
		$payment = $_POST['payment'];
		$package_id = NULL;
		$today = date('Y-m-d');

		if ($order['order_type'] == 'package') {
			$package_id = $payment['package_id'];
		}

		$combination = array('cust_id' => $cust_id, 'payment', $payment);
		$tracking_number = 'ATK'.substr(md5(uniqid(json_encode($combination), true)), 0, 8);

		$stmt = $conn->db()->prepare("INSERT INTO `order_tbl` (`cust_id`, `order_date`, `order_status`, `order_payment`, `order_type`, `package_id`, `address`, `city`, `created_at`, `order_tracking_no`) VALUES (?, ?, ?, ? ,?, ?, ?, ?, ?, ?)");

		$stmt->execute([
			$cust_id, 
			$order['order_date'],
			$order['order_status'], 
		 	$order['order_payment'], 
			$order['order_type'], 
			$package_id, 
			$order['address'], 
			$order['city'], 
			$today,
			$tracking_number]);

		if ($order['order_type'] == 'menu') {
			$this->orderLineReservation();
		}

		$this->serviceReservation();
		$this->createPayment();
		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function orderLineReservation () {
		$conn = new database();
		$order_id = $this->getReservationLastInsertId();

		if (isset($_POST['packageItem'])) {
			$order_line = $_POST['packageItem'];

			foreach ($order_line as $row) {
				$stmt = $conn->db()->prepare("INSERT INTO `orderline_tbl` (`order_id`, `food_id`, `quantity`, `price_size`, `size`) VALUES (?, ?, ?, ? ,?)");
				$stmt->execute([$order_id, $row['food_id'], $row['quantity'], $row['price_size'], $row['size']]);
			}
		}

		return true;
	}

	public function createPayment () {
		$conn = new database();		
		$order_id = $this->getReservationLastInsertId();
		$payment = $_POST['payment'];
		$cust_id = $this->getCustomerLastInsertId();

		$stmt = $conn->db()->prepare("INSERT INTO `payment_logs` (`order_id`, `cust_id`, `subtotal`, `rem_bal`, `payment_type`, `downpayment`, `amount_paid`, `payment_status`) VALUES (?, ?, ?, ? , ?, ?, ?, ?)");
		$stmt->execute([
			$order_id, 
			$cust_id, 
			$payment['subtotal'], 
			$payment['rem_bal'], 
			$payment['payment_type'],
			$payment['downpayment'],
			$payment['amount_paid'],
			$payment['payment_status']]);

		return true;
	}

	public function serviceReservation () {
		$conn = new database();
		$order_id = $this->getReservationLastInsertId();

		if (isset($_POST['utilityCart'])) {
			$utility = $_POST['utilityCart'];

			foreach ($utility as $row) {
				$stmt = $conn->db()->prepare("INSERT INTO `services` (`order_id`, `utility_id`, `quantity`) VALUES (?, ?, ?)");
				$stmt->execute([$order_id, $row['utility_id'], $row['quantity']]);
				$this->updateUtilityQuantity($row['utility_id'], $row['quantity']);
			}
		}

		return true;
	}

	public function updateUtilityQuantity ($id, $quantity) {
		$conn = new database();

		$stmt = $conn->db()->prepare("UPDATE `utilities` SET `utility_total` = `utility_total` - ? WHERE `utility_id` = ?");
		$stmt->execute([$quantity, $id]);

		return true;
	}

	public function userRegister () {
		$conn = new database();
		$customer = $_POST['customer'];

		$stmt = $conn->db()->prepare("INSERT INTO `customer` (`cust_fname`, `cust_lname`, `cust_mi`, `user_add`, `cust_cn`, `cust_email`) VALUES (?, ?, ?, ?, ?, ?)");
		$stmt->execute([$customer['firstname'], $customer['lastname'], $customer['middlename'], $customer['address'], $customer['mobilenumber'], $customer['email']]);

		return true;
	}

	public function getReservationLastInsertId () {
		$conn = new database();
		$stmt = $conn->db()->prepare("SELECT `order_id` FROM `order_tbl` ORDER BY order_id DESC LIMIT 1");
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['order_id'];		
	}

	public function getCustomerLastInsertId () {
		$conn = new database();
		$stmt = $conn->db()->prepare("SELECT `cust_id` FROM `customer` ORDER BY cust_id DESC LIMIT 1");
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['cust_id'];		
	}

	public function getEventDates () {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl`  INNER JOIN `customer` ON `order_tbl`.cust_id = `customer`.cust_id ORDER BY `created_at`");
		$stmt->execute();
		$rows = $stmt->fetchAll();

		return $rows;
	}

	public function getLimitEventDates () {
		$conn = new database();
		
		$stmt = $conn->db()->prepare("SELECT * FROM `order_tbl`  INNER JOIN `customer` ON `order_tbl`.cust_id = `customer`.cust_id ORDER BY `created_at` LIMIT 6");
		$stmt->execute();
		$rows = $stmt->fetchAll();

		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
	}

	public function updateCustomerOrderStatus () {
		$conn = new database();
		$order_id = $_POST['order_id'];
		$order_status = $_POST['order_status'];

		$stmt = $conn->db()->prepare("UPDATE `order_tbl` set `order_status` = ? WHERE order_id = ?");
		$stmt->execute([$order_status, $order_id]);

		$this->customerNotification();

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function getOrderListByAll ($type = null) {
		$conn = new database();

		if ($type) {
			$stmt = $conn->db()->prepare("SELECT * FROM ((`order_tbl` INNER JOIN `payment_logs` ON `order_tbl`.order_id = `payment_logs`.order_id) INNER JOIN `customer` ON `customer`.cust_id = `order_tbl`.cust_id) WHERE `order_status` = ?");
			$stmt->execute([$type]);
			$rows = $stmt->fetchAll();
		} else {
			$stmt = $conn->db()->prepare("SELECT * FROM ((`order_tbl` INNER JOIN `payment_logs` ON `order_tbl`.order_id = `payment_logs`.order_id) INNER JOIN `customer` ON `customer`.cust_id = `order_tbl`.cust_id)");
			$stmt->execute();
			$rows = $stmt->fetchAll();
		}
		

		return $rows;
	}
}

 ?>