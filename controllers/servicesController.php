<?php 	
require_once('../database/database.php');
require_once('utilityController.php');

class ServicesController
{
	public function getServiceList () {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM ((`order_tbl` INNER JOIN `services` ON `order_tbl`.order_id = `services`.order_id) INNER JOIN `customer` ON `customer`.cust_id = `order_tbl`.cust_id)");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	for ($index = 0; $index < count($rows); $index++) { 
    		$rows[$index]['utility_name'] = $this->getServiceUtility($rows[$index]['utility_id'])['utility_name'];
    	}
  
    	return $rows;
	}

	public function getServiceUtility ($utility_id) {
		$conn = new database();
		$utility = new utilityController();

    	return $utility->getUtility($utility_id);
	}
}

?>