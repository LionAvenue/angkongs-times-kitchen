<?php 
require_once('../database/database.php');

class UtilityController
{
	public function createUtility () {
		$conn = new database();
		$utility_name = $_POST['utility_name'];
		$utility_total = $_POST['utility_total'];
		$utility_price = $_POST['utility_price'];

		$stmt = $conn->db()->prepare("INSERT INTO `utilities` (`utility_name`, `utility_total`, `utility_price`) VALUES (?, ?, ?)");
    	$stmt->execute([$utility_name, $utility_total, $utility_price]);

		return json_encode(array('status' => 'OK', 'message' => 'success'));
	}

	public function getUtility($utility_id) {
		$conn = new database();

		$stmt = $conn->db()->prepare("SELECT * FROM `utilities` WHERE `utility_id` = ? ");
    	$stmt->execute([$utility_id]);
    	$rows = $stmt->fetch();

		return $rows;
	}

	public function getUtilityList () {
		$conn = new database();
		$dataType = isset($_POST['dataType']) ? $_POST['dataType'] : null;

		$stmt = $conn->db()->prepare("SELECT * FROM `utilities` WHERE  `is_delete` = '0'");
    	$stmt->execute();
    	$rows = $stmt->fetchAll();

    	if ($dataType == 'JSON') {
    		return json_encode(array('status' => 'OK', 'message' => 'success', 'data' => $rows));
    	}

		return $rows;
	}

	public function updateUtility () {
		$conn = new database();
		$utility_id = $_POST['utility_id'];
		$utility_name = $_POST['utility_name'];
		$utility_total = $_POST['utility_total'];
		$utility_price = $_POST['utility_price'];

		$stmt = $conn->db()->prepare("UPDATE `utilities` set `utility_name` = ?, `utility_total` = ?, `utility_price` = ? WHERE utility_id = ?");
    	$stmt->execute([$utility_name, $utility_total, $utility_price, $utility_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Utility Updated!'));
	}

	public function deleteUtility () {
		$conn = new database();
		$utility_id = $_POST['utility_id'];

		$stmt = $conn->db()->prepare("UPDATE `utilities` SET `is_delete` = '1' WHERE `utility_id` = ?");
    	$stmt->execute([$utility_id]);

		return json_encode(array('status' => 'OK', 'message' => 'Utility Deleted!'));
	}

	public function returnUtility () {
		$conn = new database();
		$utility_id = $_POST['utility_id'];
		$quantity = $_POST['quantity'];

		$stmt = $conn->db()->prepare("UPDATE `utilities` SET `utility_total` = `utility_total` + ? WHERE `utility_id` = ?");
    	$stmt->execute([$quantity, $utility_id]);

    	$this->updateServicesStatus();
		return json_encode(array('status' => 'OK', 'message' => 'Utility Returned!'));
	}

	public function updateServicesStatus () {
		$conn = new database();
		$service_id = $_POST['service_id'];
		$status = $_POST['status'];

		$stmt = $conn->db()->prepare("UPDATE `services` SET `status` = ? WHERE `service_id` = ?");
    	$stmt->execute([$status, $service_id]);

    	return json_encode(array('status' => 'OK', 'message' => 'Utility Returned!'));	
	}
	public function updateUtilityAvailability() {
		$conn = new database();

		$utility_id = $_POST['id'];
		$status = $_POST['status'];
		

		$stmt = $conn->db()->prepare("UPDATE `utilities` SET `is_available` = ? where `utility_id` = ?");
		$stmt->execute([$status , $utility_id]);

		return json_encode( array('status' => 'OK' , 'message' => 'success' )); 
	}
}



 ?>