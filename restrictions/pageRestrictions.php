<?php
require_once('../env.php');

$employee_info = $session->employee();

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
                $_SERVER['REQUEST_URI'];

$admin_restriction = array(
	"{$_ENV['base_url']}views/dashboard.php",
	"{$_ENV['base_url']}views/create-order.php", 
	"{$_ENV['base_url']}views/reservation.php", 
	"{$_ENV['base_url']}views/payment.php",
	"{$_ENV['base_url']}views/restricted.php",
	"{$_ENV['base_url']}views/order.php",
	"{$_ENV['base_url']}views/feedback.php");

if ($employee_info['emp_pos'] == 'admin') {
	if (in_array(strtok($link, '?'), $admin_restriction)) {
		return true;
	} else {
		$url = "{$_ENV['base_url']}views/restricted.php";
		 header("Location:". $url);
		die();
	}
}
?>