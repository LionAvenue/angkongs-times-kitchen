<?php 

class SessionController 
{
	public function start () {
		session_start();
	}

	public function destroy () {
		$this->start();
		session_destroy();
	}

	public function employee () {
		$this->start();

		if (!isset($_SESSION['employee'])) {
			$url = "{$_ENV['base_url_client']}views/login.php";
			header("Location:". $url);
			die();
		}

		return $_SESSION['employee'];
	}
}

?>