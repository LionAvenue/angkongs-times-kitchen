      <!-- Footer -->
      <footer id="accordionFooter" class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php require('../src/modals/modal.php');?>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo $_ENV["base_url"]?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $_ENV["base_url"]?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo $_ENV["base_url"]?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo $_ENV["base_url"]?>js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo $_ENV["base_url"]?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo $_ENV["base_url"]?>js/demo/chart-area-demo.js"></script>
  <script src="<?php echo $_ENV["base_url"]?>js/demo/chart-pie-demo.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo $_ENV["base_url"]?>vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo $_ENV["base_url"]?>vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo $_ENV["base_url"]?>js/demo/datatables-demo.js"></script>

  <!-- Full calendar -->
  <script src='<?php echo $_ENV["base_url"]?>vendor/fullcalendar/lib/moment.min.js'></script>
  <script src='<?php echo $_ENV["base_url"]?>vendor/fullcalendar/fullcalendar.min.js'></script>
  <script src="<?php echo $_ENV["base_url"]; ?>vendor/datepicker/dist/zebra_datepicker.min.js"></script>

  <script>
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {requestType: 'getNotification'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        let noti_no = data.data.length
        let template = ''
        $('#notificationNumber').text(noti_no)

        for (let index = 0; index < data.data.length; index++) {
          template = `<a class='dropdown-item d-flex align-items-center' href='<?php echo $_ENV['base_url']; ?>views/order.php?date=${data.data[index].created_at}'>
                    <div class='mr-3'>
                      <div class='icon-circle bg-primary'>
                        <i class='fas fa-file-alt text-white'></i>
                      </div>
                    </div>
                    <div>
                      <div class='small text-gray-500'>${data.data[index].created_at}</div>
                      <span class='font-weight-bold'>Customer Order</span>
                    </div>
                  </a>` + template
        }

        $('#notificationBody').append(template)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  </script>
  <script>
    function employeeLogout() {
      $.ajax({
          type: 'POST',
          url: '<?php echo $_ENV["base_url"]; ?>controllers/controller.php',
          data: {requestType: 'employeeLogout'},
          dataType: 'JSON',
          success: function (data) {
            if (data.status != 'OK') {
              swal("Error!", data.message, "warning")
              return;
            }
            
            window.location = "<?php echo $_ENV["base_url_client"]; ?>views/login.php";
          },
          error: function (data) {
            swal("Oh no!", 'Server Error', "warning")
          }
        })
    }
  </script>
</body>

</html>