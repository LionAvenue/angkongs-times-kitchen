<?php require('../env.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ATK - Admin</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo $_ENV["base_url"]?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo $_ENV["base_url"]?>css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
  <link href="<?php echo $_ENV["base_url"]?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- This is what you need -->
  <script src="<?php echo $_ENV["base_url"]?>vendor/sweetalert/dist/sweetalert.js"></script>
  <link rel="stylesheet" href="<?php echo $_ENV["base_url"]?>vendor/sweetalert/dist/sweetalert.css">
  <!--.......................-->

  <!-- Full calendar -->
  <link href='<?php echo $_ENV["base_url"]?>vendor/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='<?php echo $_ENV["base_url"]?>vendor/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  <link rel="stylesheet" href="<?php echo $_ENV["base_url"]; ?>vendor/datepicker/dist/css/bootstrap/zebra_datepicker.min.css" type="text/css">

</head>

<body id="page-top">

<!-- Page Wrapper -->
  <div id="wrapper">
  <?php require('../src/navigation/navbar.php');?>
  
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php require('../src/navigation/topbar.php');?>

