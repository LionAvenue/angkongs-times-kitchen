<?php $employee_info = $_SESSION['employee']; ?>
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo $_ENV["base_url"]?>views/dashboard.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-utensils"></i>
        </div>
        <div class="sidebar-brand-text mx-3">ATK - Admin</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/dashboard.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Order
      </div>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/create-order.php">
          <i class="fas fa-cube"></i>
          <span>Create Order</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/payment.php">
          <i class="fas fa-credit-card"></i>
          <span>Payment Logs</span></a>
      </li>

      <?php if ($employee_info['emp_pos'] != 'admin') { ?>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Food
      </div>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/menu.php">
          <i class="fas fa-utensils"></i>
          <span>Menu</span></a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/create-menu.php">
          <i class="fas fa-plus"></i>
          <span>Create Menu</span></a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/package.php">
          <i class="fas fa-archive"></i>
          <span>Package List</span></a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/create-package.php">
          <i class="fas fa-plus"></i>
          <span>Create Package</span></a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/categories.php">
          <i class="fas fa-bars"></i>
          <span>Categories</span></a>
      </li>


      <!-- <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/create-employee.php">
          <i class="fas fa-plus"></i>
          <span>Create Employee</span></a>
      </li> -->

      <?php } ?>


      <?php if ($employee_info['emp_pos'] != 'admin') { ?>
      <!-- Divider -->
      <hr class="sidebar-divider">

       <!-- Heading -->
      <div class="sidebar-heading">
        Inventory
      </div>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/utilities.php">
          <i class="fas fa-utensils"></i>
          <span>Utilities</span></a>
      </li>

     <!--  <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/create-utilities.php">
          <i class="fas fa-plus"></i>
          <span>Create Utility</span></a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/services.php">
          <i class="fas fa-book"></i>
          <span>Services</span></a>
      </li>

      <?php } ?>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Request
      </div>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/reservation.php">
          <i class="fas fa-calendar"></i>
          <span>Reservation</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/feedback.php">
          <i class="fas fa-book"></i>
          <span>Feedback</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/event.php">
          <i class="fas fa-book"></i>
          <span>Event Dates</span></a>
      </li>

      <?php if ($employee_info['emp_pos'] != 'admin') { ?>
      <!-- Heading -->
      <div class="sidebar-heading">
        Users
      </div>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/employee.php">
          <i class="fas fa-users"></i>
          <span>Employees</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $_ENV["base_url"]?>views/users.php">
          <i class="fas fa-user"></i>
          <span>Users</span></a>
      </li>

      <?php } ?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->