<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php
  require_once('../controllers/categoryController.php'); 
  $category = new categoryController(); 
  $type = 'menu';

  if (isset($_POST['categoryType'])) { 
    $type = $_POST['categoryType'];
    $categoryList = $category->getCategoryList($type);
    $categoryListObject = json_encode($categoryList);
  } else {
    $categoryList = $category->getCategoryList($type);
    $categoryListObject = json_encode($categoryList);
  }
?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Categories</h1>

          <form class="form-inline" action="categories.php" method="post">
          <div class="form-group mb-2">
                <select class="form-control" name="categoryType" id="categoryInput">
                  <option value="menu">Menu</option>
                  <option value="package">Package</option>
                  <option value="employee">Employee</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success ml-2 mb-2">Change Filter</button>
          </form>

          <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary mb-2 float-right" onclick="createCategoryModal()">New Category</button>
            </div>
          </div>



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Category Name</th>
                      <th>Category Type</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody id="categoryTable">
                  <?php foreach ($categoryList as $row) { ?>
                    <tr id="rowCategory<?php echo $row['category_id']; ?>">
                      <td><?php echo $row['category_name']; ?></td>
                      <td><?php echo $row['category_type']; ?></td>
                      <td>
                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="ViewUpdateCategory(<?php echo $row['category_id']; ?>)">View</button>
                        <button type="button" class="btn btn-danger btn-sm btn-block" onclick="deleteCategory(<?php echo $row['category_id']; ?>)">Delete</button>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script>
  let categoryType = "<?php echo $type; ?>";
  let categoryList = <?php echo $categoryListObject; ?>;
  $('#categoryInput').val(categoryType);

  function createCategoryModal () {
      $('#viewCategoryModal').modal('show');
  }

  function createCategory() {
    let new_category_name = $('#newCategoryName').val()
    let new_category_type = $('#newCategoryType').val()

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        category_name:new_category_name,
        category_type:new_category_type,
        requestType: 'createCategory'
      },
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        swal({
          title: "Awesome",
          text: "Category Created!",
          type: "success",
          confirmButtonClass: "btn-success",
          confirmButtonText: "Ok!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            location.reload();   
          }
        });

      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

  function ViewUpdateCategory(id) {
    let categoryItem = categoryList.find(function (data) {
      return parseInt(data.category_id) === id
    })

    $('#updateCategoryName').val(categoryItem.category_name)
    $('#updateCategoryType').val(categoryItem.category_type)
    $('#updateCategoryId').val(categoryItem.category_id)
    $('#viewUpdateCategoryModal').modal('show')
  }

  function updateCategory() {
    let category_name = $('#updateCategoryName').val()
    let category_type = $('#updateCategoryType').val()
    let category_id = $('#updateCategoryId').val()


    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        category_id: category_id,
        category_name:category_name,
        category_type:category_type,
        requestType: 'updateCategory'
      },
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        swal("Successful!", 'Category Updated!', "success")       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

  function deleteCategory(category_id) {
    swal({
    title: "Are you sure you want to delete this category?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          category_id: category_id, 
          requestType: 'deleteCategory'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          $(`#rowCategory${category_id}`).remove()
          swal("Deleted!", "Category deleted!", "success");      
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
  }
</script>