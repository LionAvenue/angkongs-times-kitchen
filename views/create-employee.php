<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Create Employee</h1>

          <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        
                        <input type="name" class="form-control" id="firstname" aria-describedby="emailHelp" placeholder="First Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Middle Name</label>
                        <input type="name" class="form-control" id="middlename" aria-describedby="emailHelp" placeholder="Middle Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="name" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Last Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Employee Category</label>
                        <select class="form-control" id="category" onclick="checkCategory()">
                        </select>
                      </div>
                      <div class="form-group" id="formPassword" style="display: none;">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Password" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mobile number</label>
                        <input type="number" class="form-control" id="mobilenumber"  min="0"aria-describedby="emailHelp" placeholder="Mobile Number" required>
                      </div>
                    </form>
                  </div>
                </div>
              <button type="button" class="btn btn-primary" onclick="createEmployee()">Create</button>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'employee', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        console.log(data)
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);

        if($('#category').val() == 'admin') {
           $('#formPassword').show()
        }
      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

  function checkCategory () {
    if($('#category').val() == 'admin') {
      $('#formPassword').show()
      return;
    }

    $('#formPassword').hide()
  }


  function createEmployee () {
    let regx1 = "^[a-zA-Z]+$" ; 
    let firstname = $('#firstname').val()
    let middlename = $('#middlename').val()
    let lastname = $('#lastname').val()
    let email = $('#email').val()
    let position = $('#category').val()
    let mobilenumber = $('#mobilenumber').val()
    let password = $('#password').val() ? $('#password').val() : '' 

    !firstname ? $('#firstname').css({'border': '1px solid red'}) : $('#firstname').css({'border': '1px solid green'})
    !middlename ? $('#middlename').css({'border': '1px solid red'}) : $('#middlename').css({'border': '1px solid green'})
    !lastname ? $('#lastname').css({'border': '1px solid red'}) : $('#lastname').css({'border': '1px solid green'})
    !email ? $('#email').css({'border': '1px solid red'}) : $('#email').css({'border': '1px solid green'})
    !mobilenumber ? $('#mobilenumber').css({'border': '1px solid red'}) :  $('#mobilenumber').css({'border': '1px solid green'})
    !password ? $('#password').css({'border': '1px solid red'}) : $('#password').css({'border': '1px solid green'})

    if (!firstname.match(regx1) || firstname.trim()=="" || !middlename.match(regx1)
     || middlename.trim()=="" || !lastname.match(regx1) || lastname.trim()=="" || email.trim()==""
     || !position.match(regx1) || position.trim()=="" || mobilenumber.trim == "" || password.trim ==""  ) {
        swal("Hey"," No invalid inputs and Missing Fields Please ","warning")
        return;
        }else{

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        firstname: firstname,
        middlename: middlename,
        lastname: lastname,
        email: email,
        position: position,
        mobilenumber: mobilenumber,
        password: password,
        requestType: 'createEmployee'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        swal("Yeheey!", 'Employee Created!', "success")       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }

}
</script>

