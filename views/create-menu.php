<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Create Menu</h1>

          <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Menu Name</label>
                        <input type="name" class="form-control" id="menuName" aria-describedby="emailHelp" placeholder="Menu Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <select class="form-control" id="category">
                        </select>
                      </div>
                      <div class="form-group">
                        <!-- <label for="exampleInputEmail1">Price</label> -->
                        <input type="number" class="form-control" id="price" value="0.00" aria-describedby="emailHelp" placeholder="Price" required hidden="true">
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="inputEmail4">Price XS</label>
                          <input type="number" class="form-control" id="priceXs" placeholder="price" min="1">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="inputEmail4">Price S</label>
                          <input type="numer" class="form-control" id="priceSmall" placeholder="price" min="1">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="inputEmail4">Price M</label>
                          <input type="number" class="form-control" id="priceMedium" placeholder="price" min="1">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="inputEmail4">Price L</label>
                          <input type="number" class="form-control" id="priceLarge" placeholder="price" min="1">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control" id="description" rows="3" required></textarea>
                      </div>
                        <div class="form-group">
                          <label for="exampleFormControlFile1">Menu Image</label>
                          <input type="file" class="form-control-file" id="menuImage" accept="image/*">
                        </div>
                    </form>
                  </div>
                </div>
              <button type="button" class="btn btn-primary" onclick="createMenu()">Create</button>
              <a href="<?php echo $_ENV["base_url"]; ?>views/menu.php" class="btn btn-secondary">Cancel</a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'menu', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

  function createMenu () {
   // let regx1 = "^[a-zA-Z]+$" ;
    let regx1 = "[a-zA-Z][a-zA-Z ]+[a-zA-Z]$" ;
    let menuName = $('#menuName').val()
    let category = $('#category').val()
    let price = $('#price').val()
    let description = $('#description').val()

    // condition this variables
    let priceSmall = $('#priceSmall').val()
    let priceXsmall = $('#priceXs').val()
    let priceMedium = $('#priceMedium').val()
    let priceLarge = $('#priceLarge').val()
    let file_data = $('#menuImage').prop('files')[0] 

    !menuName ? $('#menuName').css({'border': '1px solid red'}) : $('#menuName').css({'border': '1px solid green'}) 
    !description ? $('#description').css({'border': '1px solid red'}) : $('#description').css({'border': '1px solid green'}) 
    !priceSmall ? $('#priceSmall').css({'border': '1px solid red'}) : $('#priceSmall').css({'border': '1px solid green'});
    !priceXsmall ? $('#priceXs').css({'border': '1px solid red'}) : $('#priceXs').css({'border': '1px solid green'});
    !priceMedium ? $('#priceMedium').css({'border': '1px solid red'}) : $('#priceMedium').css({'border': '1px solid green'});
    !priceLarge ? $('#priceLarge').css({'border': '1px solid red'}) : $('#priceLarge').css({'border': '1px solid green'})

    if (!menuName.match(regx1) || !file_data || !description) {
      swal("Hey"," No invalid inputs and Missing Fields Please ","warning")
      return;
    }

    if (!priceSmall || !priceXsmall || !priceMedium || !priceLarge) {
      swal("Hey!", 'Please enter size!', "warning")
      return;
    }

    if (priceSmall < 1 || priceXsmall < 1 || priceMedium < 1 || priceLarge < 1) {
      swal("Hey!", 'Please enter a valid price!', "warning")
      return;
    }

    // var pattern = /^([^0-9\d]*)$/;  

    let form_data = new FormData()
    form_data.append('menuName', menuName)
    form_data.append('category', category)
    form_data.append('price', price)
    form_data.append('description', description)                  
    form_data.append('menuFile', file_data)
    form_data.append('price_xsmall', priceXsmall)
    form_data.append('price_small', priceSmall)
    form_data.append('price_medium', priceMedium)
    form_data.append('price_large', priceLarge)
    form_data.append('requestType', 'createMenu')

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      contentType: false,
      cache: false,
      processData:false,
      data: form_data,
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        swal("Yeheey!", 'Menu Created!', "success")       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>

