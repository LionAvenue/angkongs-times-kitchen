<?php require('../session/sessionController.php'); $session = new sessionController(); ?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require('../controllers/menuController.php'); 
$menuListController = new menuController();
$menuListObject = json_encode($menuListController->getMenuList());
?>
<?php require_once('../controllers/utilityController.php'); 
  $utilityList = new utilityController();
  $utilityListObject = json_encode($utilityList->getUtilityList());
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Create Order</h1>

          <!-- DataTales Example -->
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Customer Info</h5>
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Firstname</label>
                          <input type="text" class="form-control" id="firstname" placeholder="Firstname">
                        </div>
                          <div class="form-group col-md-4">
                          <label for="inputPassword4">Middlename</label>
                          <input type="text" class="form-control" id="middlename" placeholder="Middlename">
                        </div>
                          <div class="form-group col-md-4">
                          <label for="inputPassword4">Lastname</label>
                          <input type="text" class="form-control" id="lastname" placeholder="Lastname">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Email</label>
                          <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Mobile Number</label>
                          <input type="text" class="form-control" id="mobilenumber" placeholder="Mobile Number">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputCity">Address</label>
                          <input type="text" class="form-control" id="address" placeholder="Address">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputCity">City</label>
                          <input type="text" class="form-control" id="city" placeholder="City">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>

          <div class="card mt-5">
            <div class="card-body">
               <h5 class="card-title">Event Address</h5>
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputCity">Address</label>
                          <input type="text" class="form-control" id="eventAddress" placeholder="Address">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputCity">City</label>
                          <input type="text" class="form-control" id="eventCity" placeholder="City">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputCity">Time</label>
                          <input id="eventDate" type="text" class="form-control" placeholder="Event Date" data-zdp_readonly_element="false">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Order Type</label>
                        <input type="text" class="form-control" id="orderType" value="menu" placeholder="City" readonly="true ">
                     <!--    <select class="form-control" id="orderType" onchange="changeOrderType()">
                          <option value="menu">menu</option>
                          <option value="package">package</option>
                        </select> -->
                      </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>

          <div class="card mt-5">
            <div class="card-body">
              <h5 class="card-title">Menu Cart</h5>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table">
                      <thead>
                        <tr class="d-flex text-left">
                          <th class="col-2">Menu</th>
                          <th class='col-2'>Name</th>
                          <th class='col-2'>Category</th>
                          <th class='col-2'>Size</th>
                          <th class='col-1'>Price</th>
                          <th class='col-3'>Quantity</th>
                        </tr>
                      </thead>
                      <tbody id="packageTableItems">
                      </tbody>
                    </table>
                    <button type="button" class="btn btn-primary float-right" onclick="menuList()">Add Menu</button>
                  </div>
                </div>
            </div>
          </div>

          <div class="accordion mt-5" id="accordionExample">
            <div class="card">
              <div class="card-header" id="headingThree" style="background-color: #fff; border-bottom: 1px solid #000">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <h5 class="card-title">Utility List</h5>
                  </button>
                </h5>
              </div>

              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col" style="width: 20%">Name</th>
                        <th scope="col" style="width: 10%">Total</th>
                        <th scope="col" style="width: 10%">Price</th>
                        <th scope="col" style="width: 25%">Qty</th>
                        <th scope="col" style="width: 20%">Option</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($utilityList->getUtilityList() as $row) { ?>
                      <tr>
                        <td><?php echo $row['utility_name']; ?></td>
                        <td><?php echo $row['utility_total']; ?></td>
                        <td><?php echo $row['utility_price']; ?></td>
                        <td>
                          <div class='input-group mb-3'>
                            <div class='input-group-prepend'>
                              <button class='btn btn-outline-secondary' type='button' onclick='utilityQuantityModifier(<?php echo $row['utility_id']?>, "-")'>-</button>
                            </div>
                            <input type='number' class='form-control utilityQuantity-control' quantity-id="<?php echo $row['utility_id']?>" id='utilityQuantity<?php echo $row['utility_id']?>' value='0' min='0' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
                             <div class='input-group-append'>
                              <button class='btn btn-outline-secondary' type='button' onclick='utilityQuantityModifier(<?php echo $row['utility_id']?>, "+")'>+</button>
                            </div>
                          </div>
                        </td>
                        <td>
                          <?php if ($row['utility_total']) { ?>
                            <button type="button" id="rmvBtn<?php echo $row['utility_id'] ?>" class="btn btn-danger btn-sm btn-block" onclick="removeUtilityItem(<?php echo $row['utility_id'] ?>)" style="display: none;">Remove</button>
                              <?php } else { ?>
                                <button type="button" class="btn btn-danger btn-sm btn-block" disabled="true">Unavailable</button>
                          <?php }?>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="card mt-5">
            <div class="card-body">
               <h5 class="card-title">Payment</h5>
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-group">
                        <label for="inputCity">Subtotal</label>
                        <input type="text" class="form-control" id="subtotal" placeholder="0" readonly="true">
                      </div>
                      <div class="form-group">
                        <label for="inputCity">Payment Status</label>
                        <select class="form-control" id="payment_status">
                          <option value="unpaid">unpaid</option>
                          <option value="paid">paid</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="inputCity">Downpayment</label>
                        <input id="downpayment" type="number" min="1" class="form-control" value="0.00">
                      </div>
                      <div class="form-group">
                        <label for="inputCity">Amount Paid</label>
                        <input id="amount_paid" type="number" min="1" class="form-control" value="0.00">
                      </div>
                      <div class="form-group">
                        <label for="inputCity">Remaining Balance</label>
                        <input id="remainingBalance" type="number" min="1" class="form-control" value="0.00" readonly="true">
                      </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>

          <div class="card mt-5 mb-5">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" class="btn btn-primary btn-block" onclick="createOrder()">Create Order</button>
                  </div>
                </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script>
let subtotal = 0;
let totalPrice = 0;
let utilityListObject = <?php echo $utilityListObject; ?>;
let utilityCart = [];

$('#eventDate').Zebra_DatePicker({
 format: 'Y-m-d h:i:A',
    direction: true
});

     
function changeOrderType () {
  orderType = $('#orderType').val()
  window.location.href = `<?php echo $_ENV["base_url"]; ?>views/create-order.php?orderType=${orderType}`;
}

  let menu = ''
  let packageItem = []

  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        requestType: 'getMenuList'
      },
      dataType: 'JSON',
      success: function (data) {
        menu = data
        
        let menu_category = [...new Set(data.data.map(item => item.category))];
        let header_template = ''

        console.log(data)

        for (let i = 0; i < menu_category.length; i++) {

          let template = ''
          for (let index = 0; index < data.data.length; index++) {
            if (menu_category[i] == data.data[index].category) {
               template = template + ' ' + `
               <table class='table table-bordered'>
                <tbody>
                  <tr>
                    <td style='width: 50%'>${data.data[index].food_name}</td>
                    <td style='width: 50%' class='text-right'>
                      <div class='btn-group' role='group' aria-label='Basic example'>
                        <button type='button' id='btnRmv${data.data[index].food_id}xs' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}xs' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}s' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}s' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}m' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}m' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}l' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}l' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L</button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>`
            }
          }

          header_template = `<div class='col-sm-6'>
          <h4>${menu_category[i]}</h4>
          ${template}
          </div>`

          $('#modal-menu-list').append(header_template);
        }
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'package', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

function createPackage () {
  // GET VALUES VIA ID
  let packageName = $('#packageName').val()
  let category = $('#category').val()
  let price = $('#price').val()
  let description = $('#description').val()

  // GET FILE IMAGE VIA ID
  let file_data = $('#packageImage').prop('files')[0]   
  
  // INITIALIZE NEW FORM
  let form_data = new FormData()

  // APPEND VALUES IN FORM DATA
  form_data.append('packageName', packageName)
  form_data.append('category', category)
  form_data.append('price', price)
  form_data.append('description', description)                  
  form_data.append('packageImage', file_data)
  form_data.append('packageItem', JSON.stringify(packageItem))
  form_data.append('requestType', 'createPackage')


  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    contentType: false,
    cache: false,
    processData:false,
    data: form_data,
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      swal("Yeheey!", 'Package Created!', "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function addPackageItem (id, price, size) {
  let template = ''
  let quantity = 1
  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }

  let package_template = {
    food_id: '',
    price_size: '',
    quantity: ''
  }

  let food =  menu.data.find(function (data) {
    return data.food_id == id
  })

  let exist = false

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === food.food_id && packageItem[index].size === size) {
      exist = true
    }
  }

  
  if (!exist) {
    package_template.food_id = id;
    package_template.quantity = quantity;
    package_template.size = size;
    package_template.price_size = price;
    packageItem.push(package_template)

    template = `<tr id="packageItemRow${food.food_id}${size_template[size]}" class='d-flex text-left'><th class='col-2'><img class='img-thumbnail' src='<?php echo $_ENV['base_url']?>/img/menu_img/${food.food_image}' width='150px' alt='Menu Image'></th> <th class='col-2'>${food.food_name}</th> <td class='col-2'>${food.category}</td> <td class='col-2'>${size}</td> <td class='col-1'>${price}</td><td class='col-3'><div class='input-group mb-3'>
    <div class='input-group-prepend'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "-")'>-</button>
    </div>
    <input type='text' class='form-control' id='menuQuantity${food.food_id}${size_template[size]}' value='1' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
    <div class='input-group-append'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "+")'>+</button>
    </div>
    </div></td></tr>`;
    $(`#btnRmv${id}${size_template[size]}`).show()
    $(`#btnAdd${id}${size_template[size]}`).hide()
    $('#packageTableItems').append(template);
  }

  totalPrice =  parseFloat(package_template.price_size) + totalPrice
  subtotal = subtotal + 1
  $('#remainingBalance').val(totalPrice.toFixed(2))
  $('#subtotal').val(subtotal)

  // let template = ''
  // let quantity = 1

  // let food = menu.data.find(function (data) {
  //   return data.food_id == id
  // })

  // let exist = packageItem.some(function (data) {
  //   return data.food_id === food.food_id;
  // });

  // if (!exist || $(`#thisItem${food.food_id}`).is(':checked')) {
  //     food.quantity = quantity
  //     totalPrice = Math.abs(parseFloat(food.price) + totalPrice)
  //     packageItem.push(food)
  //     subtotal = subtotal + 1

  //     template = `<tr id="packageItemRow${food.food_id}" class='d-flex'><th class='col-3'><img class='img-thumbnail' src='<?php // echo $_ENV['base_url']?>/img/menu_img/${food.food_image}' width='150px' alt='Menu Image'><th><th class='col-2'>${food.food_name}</th> <td class='col-2'>${food.category}</td> <td class='col-2'>${food.price}</td><td class='col-3'><div class='input-group mb-3'>
  //     <div class='input-group-prepend'>
  //     <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "-")'>-</button>
  //     </div>
  //     <input type='text' class='form-control' id='menuQuantity${food.food_id}' value='1' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
  //     <div class='input-group-append'>
  //     <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "+")'>+</button>
  //     </div>
  //     </div></td></tr>`;

  //     $('#packageTableItems').append(template);
      // $('#remainingBalance').val(totalPrice.toFixed(2))
      // $('#subtotal').val(subtotal)
  //     return;
  // }

  // if ($(`#thisItem${food.food_id}`).is(':checked') == false) {
  //   $(`#packageItemRow${food.food_id}`).remove();
  //   packageItem = packageItem.filter(menuItem => menuItem.food_id != food.food_id)
  //   totalPrice = Math.abs(parseFloat(food.price) - totalPrice)
  //   $('#remainingBalance').val(totalPrice.toFixed(2))
  //   subtotal = subtotal - 1
  //   $('#subtotal').val(subtotal)
  //   return;
  // }
}

function quantityModifier (id, size, operator) {
  let indexItem = null
  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }
  let quantity = parseInt($(`#menuQuantity${id}${size_template[size]}`).val());

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === id && packageItem[index].size === size) {
      indexItem = index
      break;
    }
  }

  if (indexItem || indexItem === 0) {
    if (operator === '+') {
      quantity = quantity + 1
      packageItem[indexItem].quantity = quantity
      $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
    }
  
    if (operator === '-') {
      if(quantity > 1) {
        quantity = quantity - 1
        packageItem[indexItem].quantity = quantity 
        $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
      }
    }
  }

  subtotal = 0
  totalPrice = 0

  for (let index = 0; index < packageItem.length; index++) {
      totalPrice = (parseFloat(packageItem[index].price_size) * parseInt(packageItem[index].quantity)) + totalPrice
      subtotal = packageItem[index].quantity + subtotal
  }

  $('#subtotal').val(subtotal)
  $('#remainingBalance').val(totalPrice.toFixed(2))
}

function addToCart (id, quantity) {
  let utility = utilityListObject.find(function (data) {
    return data.utility_id == id
  })

  let exist = utilityCart.some(function (data) {
    return data.utility_id == id
  })

  if (!exist) {
    $(`#rmvBtn${id}`).show();
    
    if (quantity) {
      utility.quantity = quantity
      $(`#utilityQuantity${id}`).val(quantity)
    }

    utilityCart.push(utility);

    total = utilityCart.length;
  }

  this.totalPaymentComputation()
}

// utilityQuantity-control
$(".utilityQuantity-control").keyup(function(){
    let id = $(this).attr('quantity-id')
    let utilityQuantity = $(this).val()

    if (utilityQuantity != 0) {
      utilityQuantityModifier(id, '+', utilityQuantity)
    } else {
      quantityModifier(id, '-', utilityQuantity)
    }
});

function utilityQuantityModifier (id, operator, qty = null) {
    let quantity = parseInt($(`#utilityQuantity${id}`).val());
    let utility = utilityListObject.find(function (item) {
        return parseInt(item.utility_id) === id;
      });

    let utilityIndex = utilityCart.findIndex(function (item) {
      return parseInt(item.utility_id) === id; 
    })

    if (parseInt(quantity) >= parseInt(utility.utility_total)) {
        swal('Wait!', 'Quantity is greater than available utility', 'info');
        return;
    }

    if (operator === '+') {
      quantity = quantity + 1
      if (utilityIndex == -1) {
        this.addToCart(id, quantity)
        return; 
      }

      utilityCart[utilityIndex].quantity = quantity
      $(`#utilityQuantity${id}`).val(quantity)
    }

    if (operator === '-') {
      if (quantity > 0) {
        quantity = quantity - 1
        utilityCart[utilityIndex].quantity = quantity
        $(`#utilityQuantity${id}`).val(quantity)
      }

      if (quantity == 0) {
        this.removeUtilityItem(id);
      }
    }

    this.totalPaymentComputation()
}

function totalPaymentComputation () {
    subtotal = 0
    totalPrice = 0
    let utility_total_price = 0
    let utility_total_subtotal = 0

    for (let index = 0; index < packageItem.length; index++) {
        totalPrice = (parseFloat(packageItem[index].price_size) * parseInt(packageItem[index].quantity)) + totalPrice
        subtotal = packageItem[index].quantity + subtotal
    }

    for (let index = 0; index < utilityCart.length; index++) {
      utility_total_price = (parseFloat(utilityCart[index].utility_price) * parseInt(utilityCart[index].quantity)) + utility_total_price
    }

    totalPrice = utility_total_price + totalPrice

    $('#subtotal').val(subtotal)
    $('#remainingBalance').val(totalPrice.toFixed(2))
}

function removePackageItem (id, price, size) {
  let indexItem = null

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === id && packageItem[index].size === size) {
      indexItem = index
      break;
    }
  }

  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }

  if (indexItem || indexItem == 0) {
    $(`#btnRmv${id}${size_template[size]}`).hide()
    $(`#btnAdd${id}${size_template[size]}`).show()
    $(`#packageItemRow${id}${size_template[size]}`).remove()
    packageItem.splice(indexItem, 1);


    subtotal = 0
    totalPrice = 0

    for (let index = 0; index < packageItem.length; index++) {
        totalPrice = (parseFloat(packageItem[index].price_size) * parseInt(packageItem[index].quantity)) + totalPrice
        subtotal = packageItem[index].quantity + subtotal
    }

    $('#subtotal').val(subtotal)
    $('#remainingBalance').val(totalPrice.toFixed(2))
  }
}

function removeUtilityItem (id) {
  let exist = utilityCart.some(function (data) {
    return data.utility_id == id
  })

  if (exist) {
    $(`#atcBtn${id}`).show();
    $(`#rmvBtn${id}`).hide();
    $(`#utilityQuantity${id}`).val(0)
    utilityCart = utilityCart.filter(utilityItem => utilityItem.utility_id != id)
  }
}

function createOrder () {
 let payment = {
    payment_type: 'Over The Counter',
    payment_status: $('#payment_status').val(),
    subtotal: $('#subtotal').val(),
    downpayment: $('#downpayment').val(),
    rem_bal: $('#remainingBalance').val(),
    amount_paid: $('#amount_paid').val()
  }

  let customer = {
     firstname: $('#firstname').val(),
     lastname: $('#lastname').val(),
     middlename: $('#middlename').val(),
     address: $('#address').val(),
     mobilenumber: $('#mobilenumber').val(),
     email: $('#email').val(),
     city: $('#city').val()
  }

  let order = {
    order_payment: $('#remainingBalance').val(),
    order_date: $('#eventDate').val(),
    order_type: 'menu',
    order_status: 'pending',
    notif_status: '0',
    address: $('#eventAddress').val(),
    city: $('#eventCity').val()
  }

  !payment.payment_status ? $('#payment_status').css({'border': '1px solid red'}) : $('#payment_status').css({'border': '1px solid green'});
  !payment.subtotal ? $('#subtotal').css({'border': '1px solid red'}) : $('#subtotal').css({'border': '1px solid green'});
  !payment.downpayment ? $('#downpayment').css({'border': '1px solid red'}) : $('#downpayment').css({'border': '1px solid green'});
  !payment.rem_bal ? $('#remainingBalance').css({'border': '1px solid red'}) : $('#remainingBalance').css({'border': '1px solid green'});
  !payment.amount_paid ? $('#amount_paid').css({'border': '1px solid red'}) : $('#amount_paid').css({'border': '1px solid green'});


  // delete this just incase
  if (parseFloat(payment.amount_paid) > payment.rem_bal) {
    swal('Wait!', 'Money is greater than Remaining Balancer', 'info')
    return;
  }

  payment.rem_bal = Math.abs(parseFloat(payment.amount_paid) - payment.rem_bal)

  if (parseFloat(downpayment) > parseFloat(payment.rem_bal)) {
      swal('Wait!', 'Money is greater than Remaining Balance', 'info')
      return;
  }

  if (payment.downpayment > payment.rem_bal && payment.rem_bal != 0) {
    swal('Wait!', 'Money is greater than Remaining Balancer', 'info')
    return;
  }

  if (payment.downpayment != 0.00 && payment.rem_bal != 0) {
      payment.rem_bal = Math.abs(parseFloat(payment.rem_bal.toFixed(2)) - payment.downpayment)
  }

  console.log(payment.rem_bal)
  $('#remainingBalance').val(payment.rem_bal)
  // end here

  // if (parseFloat(payment.amount_paid) > parseFloat(payment.rem_bal)) {
  //   swal("Wait!", 'Amount Paid is Greater Than Remaining Balance!', "warning")
  //   return;
  // }

  // if (packageItem.length == 0) {
  //   swal("Wait!", 'Your Cart Is Empty!', "warning")
  //   return;
  // }

  // if (payment.length == 0) {
  //   swal("Wait!", 'Please Complete your Payment Information', "warning")
  //   return;
  // }
  
  // if (totalPrice !== 0.00 || totalPrice !== 0) {
  //     if (parseFloat(payment.amount_paid) > totalPrice || parseFloat(payment.downpayment) > totalPrice) {
  //       swal('Wait!', 'Money is greater than Remaining Balance', 'info')
  //       return;
  //     }

  //     totalPrice = Math.abs(payment.amount_paid - totalPrice)

  //     if (payment.downpayment > totalPrice && totalPrice != 0) {
  //       swal('Wait!', 'Money is greater than Remaining Balancer', 'info')
  //       return;
  //     }

  //     if (payment.downpayment != 0.00 && totalPrice != 0) {
  //         totalPrice = Math.abs(parseFloat(totalPrice) - parseFloat(payment.downpayment))
  //     }

  //     $('#remainingBalance').val(totalPrice.toFixed(2))
  // }

  !customer.firstname ? $('#firstname').css({'border': '1px solid red'}) : $('#firstname').css({'border': '1px solid green'})
  !customer.middlename ? $('#lastname').css({'border': '1px solid red'}) : $('#lastname').css({'border': '1px solid green'})
  !customer.lastname ? $('#middlename').css({'border': '1px solid red'}) : $('#middlename').css({'border': '1px solid green'})
  !customer.email ? $('#email').css({'border': '1px solid red'}) : $('#email').css({'border': '1px solid green'})
  !customer.address ? $('#address').css({'border': '1px solid red'}) : $('#address').css({'border': '1px solid green'})
  !customer.mobilenumber ? $('#mobilenumber').css({'border': '1px solid red'}) : $('#mobilenumber').css({'border': '1px solid green'})
  !customer.city ? $('#city').css({'border': '1px solid red'}) : $('#city').css({'border': '1px solid green'})

  !order.order_payment ? $('#remainingBalance').css({'border': '1px solid red'}) : $('#remainingBalance').css({'border': '1px solid green'})
  !order.order_date ? $('#eventDate').css({'border': '1px solid red'}) : $('#eventDate').css({'border': '1px solid green'})
  !order.address ? $('#eventAddress').css({'border': '1px solid red'}) : $('#eventAddress').css({'border': '1px solid green'})
  !order.city ? $$('#eventCity').css({'border': '1px solid red'}) : $('#eventCity').css({'border': '1px solid green'})


  if (!customer.firstname || !customer.middlename || !customer.lastname || !customer.email || !customer.address || !customer.mobilenumber) {
      swal("Hey!", 'Please enter your information!', "warning")
      return;
  }

  if (!order.order_payment || !order.order_date || !order.order_type || !order.order_status || !order.notif_status || !order.address || !order.city) {
      swal("Wait!", 'Please Complete your Order Information!', "warning")
      return;
  }

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        customer: customer,
        payment: payment,
        order: order,
        packageItem: packageItem,
        utilityCart: utilityCart,
        requestType: 'createOrder'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        swal("Successfully!", "Created Order", "success")
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>