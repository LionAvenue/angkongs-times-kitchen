<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Create Package</h1>

          <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <form>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Package Name</label>
                        <input type="name" class="form-control" id="packageName" aria-describedby="emailHelp" placeholder="Package Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <select class="form-control" id="category">
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="number" class="form-control" id="price" aria-describedby="emailHelp" placeholder="Price" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Limit Per Head</label>
                        <input type="number" class="form-control" min="1" id="limit" aria-describedby="emailHelp" placeholder="Limit" required>
                      </div>
                       <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control" id="description" rows="3" required></textarea>
                      </div>
                       <div class="form-group">
                          <label for="exampleFormControlFile1">Package Image</label>
                          <input type="file" class="form-control-file" id="packageImage" accept="image/*">
                        </div>
                    </form>
                  </div>
                  <div class="col-md-8">
                    <h3>Package</h3>
                    <table class="table">
                      <thead>
                        <tr class="d-flex">
                          <th class='col-3'>Menu Name</th>
                          <th class='col-2'>Category</th>
                          <th class='col-2'>Size</th>
                          <th class='col-2'>Price</th>
                          <th class='col-3'>Quantity</th>
                        </tr>
                      </thead>
                      <tbody id="packageTableItems">
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
              <div class="card-footer">
                <button type="button" class="btn btn-primary float-left" onclick="createPackage()">Create</button>
                <a href="<?php echo $_ENV["base_url"]; ?>views/package.php" class="btn btn-secondary float-left" style="margin-left: 5px">Cancel</a>
                <button type="button" class="btn btn-primary float-right" onclick="menuList()">Add Menu</button>
              </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script type="text/javascript">
  let menu = ''
  let packageItem = []

  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        requestType: 'getMenuList'
      },
      dataType: 'JSON',
      success: function (data) {
        menu = data
        
        let menu_category = [...new Set(data.data.map(item => item.category))];
        let header_template = ''

        for (let i = 0; i < menu_category.length; i++) {

          let template = ''
          for (let index = 0; index < data.data.length; index++) {
            if (menu_category[i] == data.data[index].category) {
               template = template + ' ' + `
               <table class='table table-bordered'>
                <tbody>
                  <tr>
                    <td style='width: 50%'>${data.data[index].food_name}</td>
                    <td style='width: 50%' class='text-right'>
                      <div class='btn-group' role='group' aria-label='Basic example'>
                        <button type='button' id='btnRmv${data.data[index].food_id}xs' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}xs' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}s' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}s' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}m' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}m' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}l' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}l' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L</button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>`
            }
          }

          header_template = `<div class='col-md-12'>
          <h4 class='text-capitalize'>${menu_category[i]}</h4>
          ${template}
          </div>`

          $('#modal-menu-list').append(header_template);
        }
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'package', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

function createPackage () {
   let regx1 = "^[a-zA-Z]+$" ;
  // GET VALUES VIA ID
  let packageName = $('#packageName').val()
  let category = $('#category').val()
  let price = $('#price').val()
  let description = $('#description').val()
  let limit = $('#limit').val()

  // GET FILE IMAGE VIA ID
  let file_data = $('#packageImage').prop('files')[0]   
  
   if (!packageName.match(regx1) || price.trim() == "" || price < 1 || !file_data || description.trim()=="") {
        swal("Hey"," No invalid inputs and Missing Fields Please ","warning")
        return;
      }else{
  // INITIALIZE NEW FORM
  let form_data = new FormData()

  // APPEND VALUES IN FORM DATA
  form_data.append('packageName', packageName)
  form_data.append('category', category)
  form_data.append('price', price)
  form_data.append('limit', limit)
  form_data.append('description', description)                  
  form_data.append('packageImage', file_data)
  form_data.append('packageItem', JSON.stringify(packageItem))
  form_data.append('requestType', 'createPackage')

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    contentType: false,
    cache: false,
    processData:false,
    data: form_data,
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      swal("Yeheey!", 'Package Created!', "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}
}
function addPackageItem (id, price, size) {
  let template = ''
  let quantity = 1
  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }

  let package_template = {
    food_id: '',
    price_size: '',
    quantity: ''
  }

  let food =  menu.data.find(function (data) {
    return data.food_id == id
  })

  let exist = false

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === food.food_id && packageItem[index].size === size) {
      exist = true
    }
  }

  
  if (!exist) {
    package_template.food_id = id;
    package_template.quantity = quantity;
    package_template.size = size;
    package_template.price_size = price;
    packageItem.push(package_template)

    template = `<tr id="packageItemRow${food.food_id}${size_template[size]}" class='d-flex'><th class='col-3'>${food.food_name}</th> <td class='col-2'>${food.category}</td> <td class='col-2'>${size}</td> <td class='col-2'>${price}</td><td class='col-3'><div class='input-group mb-3'>
    <div class='input-group-prepend'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "-")'>-</button>
    </div>
    <input type='text' class='form-control' id='menuQuantity${food.food_id}${size_template[size]}' value='1' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
    <div class='input-group-append'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "+")'>+</button>
    </div>
    </div></td></tr>`;
    $(`#btnRmv${id}${size_template[size]}`).show()
    $(`#btnAdd${id}${size_template[size]}`).hide()
    $('#packageTableItems').append(template);
  }
}

function removePackageItem (id, price, size) {
  let indexItem = null

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === id && packageItem[index].size === size) {
      indexItem = index
      break;
    }
  }

  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }

  if (indexItem || indexItem == 0) {
    $(`#btnRmv${id}${size_template[size]}`).hide()
    $(`#btnAdd${id}${size_template[size]}`).show()
    $(`#packageItemRow${id}${size_template[size]}`).remove()
    packageItem.splice(indexItem, 1);
  }
}

function quantityModifier (id, size, operator) {
  let indexItem = null
  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }
  let quantity = parseInt($(`#menuQuantity${id}${size_template[size]}`).val());

  for (let index = 0; index < packageItem.length; index ++) {
    if (packageItem[index].food_id === id && packageItem[index].size === size) {
      indexItem = index
      break;
    }
  }

  if (indexItem || indexItem === 0) {
    if (operator === '+') {
      quantity = quantity + 1
      packageItem[indexItem].quantity = quantity
      $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
    }
  
    if (operator === '-') {
      if(quantity > 1) {
        quantity = quantity - 1
        packageItem[indexItem].quantity = quantity 
        $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
      }
    }
  }
}
</script>

