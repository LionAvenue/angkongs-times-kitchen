<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Create Utility</h1>

          <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Utility Name</label>
                        <input type="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Utility Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Utility Total</label>
                        <input type="number" class="form-control" id="total" aria-describedby="emailHelp" placeholder="Quantity" min="1" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Utility Price</label>
                        <input type="number" class="form-control" id="price" aria-describedby="emailHelp" placeholder="Price" min="1" required>
                      </div>
                    </form>
                  </div>
                </div>
              <button type="button" class="btn btn-primary" onclick="createUtility()">Create</button>
              <a href="<?php echo $_ENV["base_url"]; ?>views/utilities.php" class="btn btn-secondary">Cancel</a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script type="text/javascript">
  function createUtility () {
    let regx1 = "^[a-zA-Z]+$" ; 
    let utility_name = $('#name').val()
    let utility_total = $('#total').val()
    let utility_price = $('#price').val()

    if (utility_name.trim() =="" || utility_price.trim() == "" || utility_price < 1 || utility_total.trim()=="") {
        swal("Hey"," No invalid inputs and Missing Fields Please ","warning")
        return; 
      }else{
    let form_data = new FormData()

    form_data.append('utility_name', utility_name) 
    form_data.append('utility_total', utility_total)
    form_data.append('utility_price', utility_price)                     
    form_data.append('requestType', 'createUtility')

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      contentType: false,
      cache: false,
      processData:false,
      data: form_data,
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        swal("Yeheey!", 'Utility Created!', "success")       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }
}
</script>

