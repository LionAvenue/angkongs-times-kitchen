<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php'); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
          </div>

          <div class="row mb-5">
            <div class="col-md-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Events</h5>
                  <div class="list-group mb-3" id="eventListTemplate">
                  </div>
                  <a href="<?php echo $_ENV["base_url"]?>views/event.php" class="btn btn-primary">View All Events</a>
                </div>
              </div>
            </div>
          </div>

          <div id="feedbackTemplate" class="row">
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

    <!-- Modal -->
      <div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Customer Event</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Full name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="customerName" disabled="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-sm-2 col-form-label">Event Date</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="eventDate" disabled="true">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword" class="col-sm-2 col-form-label">Event Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="eventAddress" disabled="true">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

  <script>
  let evetList = [];

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {requestType: 'getLimitEventDates'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        console.log(data)
        let template = ''

        for (let index = 0; index < data.data.length; index++) {
          evetList.push(data.data[index]);
          template = `<button type='button' class='list-group-item list-group-item-action' onclick='viewEvents(${data.data[index].order_id})'>${data.data[index].order_date} | ${data.data[index].cust_fname} ${data.data[index].cust_lname}</button>` + template
        }
        
        $('#eventListTemplate').append(template)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })


    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {requestType: 'getLimitFeedbackList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        console.log(data)
        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<div class='col-md-4 mb-3'>
              <div class='card'>
                <div class='card-body'>
                  <h5 class='card-title'>Feedback</h5>
                  <p>${data.data[index].user_name}</p>
                  <p>${data.data[index].message}</p>
                  <b>Rate ${data.data[index].rating}</b>
                </div>
              </div>
            </div>` + template
        }

        $('#feedbackTemplate').append(template)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

  function viewEvents (id) {
    let customerEvent = evetList.find(data => data.order_id == id)

    $('#eventModal').modal('show')
    let fullname = `${customerEvent.cust_fname} ${customerEvent.cust_lname}`
    $("#customerName").val(fullname)
    $("#eventDate").val(customerEvent.order_date)
    $("#eventAddress").val(customerEvent.address)
  }  
  </script>