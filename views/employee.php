<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/employeeController.php'); 
  $employeeList = new employeeController();
  $employeeListObject = json_encode($employeeList->getEmployeeList());
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Employee List</h1>
          <a href="<?php echo $_ENV["base_url"]?>views/create-employee.php" class="btn btn-primary mb-3">Create Employee</a>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Firstname</th>
                      <th>Middlename</th>
                      <th>Lastname</th>
                      <th>Position</th>
                      <th>Email</th>
                      <th>Mobile No.</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($employeeList->getEmployeeList() as $row) { ?>
                  <tr id="rowEmployee<?php echo $row['emp_id']; ?>">
                     <td id="rowEmployeeFname<?php echo $row['food_id']; ?>"><?php echo $row['emp_fname']; ?></td>
                     <td id="rowEmployeeMi<?php echo $row['food_id']; ?>"><?php echo $row['emp_mi']; ?></td>
                     <td id="rowEmployeeLname<?php echo $row['food_id']; ?>"><?php echo $row['emp_lname']; ?></td>
                     <td id="rowEmployeePos<?php echo $row['food_id']; ?>"><?php echo $row['emp_pos']; ?></td>
                     <td id="rowEmployeeEmail<?php echo $row['food_id']; ?>"><?php echo $row['emp_email']; ?></td>
                     <td id="rowEmployeeNumber<?php echo $row['food_id']; ?>"><?php echo $row['emp_number']; ?></td>
                     <td>
                     <button type='button' class='btn btn-primary btn-sm btn-block' onclick='viewEmployee(<?php echo $row['emp_id']; ?>)'>View</button> 
                     <button type='button' class='btn btn-danger btn-sm btn-block' onclick='deleteEmployee(<?php echo $row['emp_id']; ?>)'>Delete</button> 
                     </td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


      <!-- Modal -->
      <div class="modal fade" id="viewEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">View Employee</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input type="name" class="form-control" id="employee_id" aria-describedby="emailHelp" placeholder="First Name" required hidden>
                <div class="form-group">
                  <label for="exampleInputEmail1">First Name</label>
                  <input type="name" class="form-control" id="firstname" aria-describedby="emailHelp" placeholder="First Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Middle Name</label>
                  <input type="name" class="form-control" id="middlename" aria-describedby="emailHelp" placeholder="Middle Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Last Name</label>
                  <input type="name" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Last Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Employee Category</label>
                  <select class="form-control" id="category">
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Mobile number</label>
                  <input type="name" class="form-control" id="mobilenumber" aria-describedby="emailHelp" placeholder="Mobile Number" required>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="updateEmployee()">Save changes</button>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<script>
let employeeListObject = <?php echo $employeeListObject; ?>;

$(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'employee', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

function updateEmployee () {
  let firstname = $('#firstname').val()
  let lastname = $('#lastname').val()
  let middlename = $('#middlename').val()
  let position = $('#category').val()
  let email = $('#email').val()
  let mobilenumber = $('#mobilenumber').val()
  let employee_id =$('#employee_id').val()

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {
        firstname: firstname,
        lastname: lastname,
        middlename: middlename,
        position: position,
        email: email,
        mobilenumber: mobilenumber,
        employee_id: employee_id,
        requestType: 'updateEmployee'
    },
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      // $(`#rowFoodName${food_id}`).text(menuName)
      // $(`#rowFoodDesc${food_id}`).text(description)
      // $(`#rowFoodPrice${food_id}`).text(price)
      // $(`#rowFoodCategory${food_id}`).text(category)
      swal("Yes!", data.message, "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function viewEmployee (emp_id) {
  let employee = employeeListObject.find(function (employee) {
    return employee.emp_id == emp_id
  })

  $('#firstname').val(employee.emp_fname)
  $('#middlename').val(employee.emp_mi)
  $('#lastname').val(employee.emp_lname)
  $('#email').val(employee.emp_email)
  $('#category').val(employee.emp_pos)
  $('#mobilenumber').val(employee.emp_number)
  $('#employee_id').val(employee.emp_id)
  $('#viewEmployeeModal').modal('show')
}

function deleteEmployee (emp_id) {
    swal({
    title: "Are you sure?",
    text: "Your will not be able to recover this imaginary file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          emp_id: emp_id, 
          requestType: 'deleteEmployee'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          $(`#rowEmployee${emp_id}`).remove()
          swal("Deleted!", "Your imaginary file has been deleted.", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
}
</script>