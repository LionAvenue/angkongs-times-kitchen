<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Feedback</h1>

          <div class="list-group">
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div class="modal fade" id="readFeedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <h5 id="sender"></h5>
              <small id="date_sent"></small>
              <hr>
              <p id="message"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<style type="text/css">
  .cut-text { 
    text-overflow: ellipsis;
    overflow: hidden;  
    height: 1.2em; 
    white-space: nowrap;
  }
</style>

<script>
let feedback = []
    $(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {requestType: 'getFeedbackList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        feedback = data.data
        let sort_date = ([...new Set(data.data.map(item => item.date_sent))]).reverse();


        for (let x = 0; x < sort_date.length; x++) {
            let header = ''
            header = `<h5 class='mt-5' id="header-group${sort_date[x]}">${sort_date[x]}</h5>` + header
            $('.list-group').append(header);

            for (let index = 0; index < data.data.length; index++) {
              if (sort_date[x] == data.data[index].date_sent) {
                  let template = '' 
                  template = `<a href='#' onclick='readFeedback(${data.data[index].feedback_id})' class='list-group-item list-group-item-action flex-column align-items-start'>
                  <div class='d-flex w-100 justify-content-between'>
                    <h5 class='mb-1'>${data.data[index].user_name}</h5>
                  </div>
                  <p class='mb-1 cut-text'>${data.data[index].feedback_message}</p>
                </a>` + template
                $('.list-group').append(template)
              }
            }
        }

        // $('.list-group').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

function readFeedback (id) {
    let feedbackItem = feedback.find(function (item) {
      return parseInt(item.feedback_id) === id;
  });


  $('#sender').text(`From: ${feedbackItem.user_name}`)
  $('#date_sent').text(feedbackItem.date_sent)
  $('#message').text(feedbackItem.feedback_message)
  $('#readFeedbackModal').modal('show')
}
</script>