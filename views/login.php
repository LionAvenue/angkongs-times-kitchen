<?php
$email = "";
$password = "";

  if (isset($_GET['email']) && isset($_GET['password'])) {
    $email = $_GET['email'];
    $password = $_GET['password'];
  }
?>
<?php require('../src/layouts/header.php');?>
<?php require('../src/layouts/footer.php');?>
<script>
  let email = "<?php echo $email ?>";
  let password = "<?php echo $password ?>";

  if (email && password) {
    employeelogin(email, password)
  } else {
    window.location.href = "<?php echo $_ENV["base_url_client"]?>views/login.php";
  }

  function employeelogin () {

    if (!email || !password) {
      swal("Hey!", 'Please enter email and password!', "warning")
      return;
    }

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {email: email, password: password, requestType: 'employeeLogin'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }

        localStorage.setItem('employee', JSON.stringify(data.data))
        window.location.href = "<?php echo $_ENV["base_url"]?>views/dashboard.php";        
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }
</script>