<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/menuController.php'); 
  $menuList = new menuController();
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Menu List</h1>
          <a href="<?php echo $_ENV["base_url"]?>views/create-menu.php" class="btn btn-primary mb-3">Create Menu</a>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Price Xs</th>
                      <th>Price S</th>
                      <th>Price M</th>
                      <th>Price L</th>
                      <th>Category</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($menuList->getMenuList() as $row) { ?>
                  <tr id="rowFood<?php echo $row['food_id']; ?>">
                     <td id="rowFoodName<?php echo $row['food_id']; ?>"><?php echo $row['food_name']; ?></td>
                     <td id="rowFoodDesc<?php echo $row['food_id']; ?>"><?php echo $row['food_desc']; ?></td>
                     <td id="rowFoodPrice<?php echo $row['food_id']; ?>"><?php echo $row['price_xsmall']; ?></td>
                     <td id="rowFoodPrice<?php echo $row['food_id']; ?>"><?php echo $row['price_small']; ?></td>
                     <td id="rowFoodPrice<?php echo $row['food_id']; ?>"><?php echo $row['price_medium']; ?></td>
                     <td id="rowFoodPrice<?php echo $row['food_id']; ?>"><?php echo $row['price_large']; ?></td>
                     <td id="rowFoodCategory<?php echo $row['food_id']; ?>"><?php echo $row['category']; ?></td>
                     <td>
                     <button type='button' class='btn btn-primary btn-sm btn-block' onclick='viewMenu(<?php echo $row['food_id']; ?>)'>View</button> 
                     <button type='button' class='btn btn-danger btn-sm btn-block' onclick='deleteMenu(<?php echo $row['food_id']; ?>)'>Delete</button> 
                     <button type='button' id ="temporaryUnavailable<?php echo $row['food_id']; ?>" class='btn btn-warning btn-sm btn-block' onclick='updateAvailability(<?php echo $row['food_id']; ?> , 1)' <?php echo $row['is_available'] == '1' ? "style='display:none'": "display" ?>>Temporary Unavailable</button> 
                     <button type='button' id = "available<?php echo $row['food_id']; ?>" class='btn btn-primary btn-sm btn-block' onclick='updateAvailability(<?php echo $row['food_id']; ?>, 0)' <?php echo $row['is_available'] == '0' ? "style='display:none'": "display" ?>>Available</button>           
                     </td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div class="modal fade" id="viewMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">View Menu</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <input type="hidden" id="food_id">
                <img id="menuImage" src="../img/menu_img/not-available.png" alt=" Image Not Found!" width="200" height="200" class="img-thumbnail mx-auto d-block mb-5">
                <div class="form-group">
                  <label for="exampleFormControlFile1">Menu Image</label>
                  <input type="file" class="form-control-file" id="inputFoodImage" accept="image/*">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Menu Name</label>
                  <input type="name" class="form-control" id="inputMenuName" aria-describedby="emailHelp" placeholder="Menu Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" id="inputCategory">
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price Xsmall</label>
                  <input type="number" class="form-control" id="inputPriceXs" aria-describedby="emailHelp" placeholder="Price" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price Small</label>
                  <input type="number" class="form-control" id="inputPriceSmall" aria-describedby="emailHelp" placeholder="Price" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price Medium</label>
                  <input type="number" class="form-control" id="inputPriceMedium" aria-describedby="emailHelp" placeholder="Price" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price Large</label>
                  <input type="number" class="form-control" id="inputPriceLarge" aria-describedby="emailHelp" placeholder="Price" required>
                </div>
                 <div class="form-group">
                  <label for="exampleFormControlTextarea1">Description</label>
                  <textarea class="form-control" id="inputDescription" rows="3" required></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="updateMenu()">Save changes</button>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<script>
$(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'menu', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#inputCategory').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  })

function viewMenu (id) {
   $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {id: id, requestType: 'getMenu'},
    dataType: 'JSON',
    success: function (data) {

      $('#viewMenuModal').modal('show')
      $('#inputMenuName').val(data.data.food_name)
      $('#inputCategory').val(data.data.category)
      $('#inputDescription').val(data.data.food_desc)
      $('#inputPriceXs').val(data.data.price_xsmall)
      $('#inputPriceSmall').val(data.data.price_small)
      $('#inputPriceMedium').val(data.data.price_medium)
      $('#inputPriceLarge').val(data.data.price_large)
      $('#menuImage').attr('src', '<?php echo $_ENV['base_url']; ?>/img/menu_img/'+ data.data.food_image)
      $('#menuImage').val(data.data.food_image)
      $('#food_id').val(data.data.food_id)
    },
    error: function (data) {
      swal("Oh no!", "You clicked the button!", "warning")
    }
  })
}


function updateMenu () {
  // GET VALUES VIA #ID
  let regx1 = "^[a-zA-Z]+$" ;
  let menuName = $('#inputMenuName').val()
  let category = $('#inputCategory').val()
  let description = $('#inputDescription').val()
  let price = $('#inputPrice').val()
  let food_id = $('#food_id').val()
  let current_img = $('#menuImage').val();
  let priceSmall = $('#inputPriceSmall').val()
  let priceXsmall = $('#inputPriceXs').val()
  let priceMedium = $('#inputPriceMedium').val()
  let priceLarge = $('#inputPriceLarge').val()

  if(!menuName.match(regx1) || !description || !priceSmall || !priceXsmall || !priceMedium || !priceLarge || !current_img){
     swal("Hey!", 'Please input missing fields!', "warning")
     return;       
  }

  if(priceSmall < 1 || priceXsmall < 1 || priceMedium < 1 || priceLarge < 1){
     swal("Hey!", 'Invalid input!', "warning")
     return;       
  }

  // GET FILE VIE ID
  let file_data = $('#inputFoodImage').prop('files')[0] 

  // CREATING NEW FORM
  let form_data = new FormData()

  // APPEND VALUES IN FORM
  form_data.append('food_id', food_id)
  form_data.append('menuName', menuName)
  form_data.append('category', category)
  form_data.append('price', price)
  form_data.append('description', description)  
  form_data.append('current_img', current_img);
  form_data.append('price_xsmall', priceXsmall)
  form_data.append('price_small', priceSmall)
  form_data.append('price_medium', priceMedium)
  form_data.append('price_large', priceLarge)                
  form_data.append('requestType', 'updateMenu')

  // IF THERE IS A NEW MENU IMAGE APPEND FILE
  if (file_data) {
    form_data.append('menuFile', file_data)
  }

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: form_data,
    contentType: false,
    cache: false,
    processData:false,
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      $(`#rowFoodName${food_id}`).text(menuName)
      $(`#rowFoodDesc${food_id}`).text(description)
      $(`#rowFoodPrice${food_id}`).text(price)
      $(`#rowFoodCategory${food_id}`).text(category)
      
      // IF THERE IS A NEW MENU IMAGE FILE CHANGE IMG SRC
      if (file_data) {
        $('#menuImage').attr('src', '<?php echo $_ENV['base_url']; ?>img/menu_img/' + file_data.name)
      }

      swal("Yes!", data.message, "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function deleteMenu (food_id) {
    swal({
    title: "Are you sure you want to delete this?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          food_id: food_id,
          hello: 'hello', 
          requestType: 'deleteMenu'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          $(`#rowFood${food_id}`).remove()
          swal("Deleted!", "Menu deleted!", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
}

function updateAvailability(id,status) {
 // console.log(id);
  //console.log(status);
 
  swal({
   title: "Are you sure?",
    text: "You want to change the availability status",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, Update Status!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          id:id,
          status: status, 
          requestType: 'updateAvailability'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          if (status == 0) {
            $(`#temporaryUnavailable${id}`).show()
            $(`#available${id}`).hide()

          }

          if (status == 1) {
            $(`#temporaryUnavailable${id}`).hide()
            $(`#available${id}`).show()
          }
          swal("Successfully!", "You have changed the availability status.", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
 

}
</script>