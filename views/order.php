<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/reservationController.php');
  $reservation = new reservationController();
  $reservationList = array();
  $date = "";

  if (isset($_GET['date'])) {
    $date = $_GET['date'];
    $reservationList = $reservation->getOrderListByDate($date);
  }
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Orders Of <?php echo $date; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Customer Name</th>
                      <th>Order Type</th>
                      <th>Order Status</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>Payment</th>
                      <th>Status Option</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($reservationList as $row) { ?>
                    <tr id="orderTableRow<?php echo $row['order_id']?>" class="<?php echo $row['notif_status'] == 1 ? 'table-success' : 'table-warning' ;?>">
                        <td><?php echo $row['cust_fname'] .' '. $row['cust_lname']; ?></td>
                        <td><?php echo $row['order_type']; ?></td>
                        <td><?php echo $row['order_status']; ?></td>
                        <td><?php echo $row['address']; ?></td>
                        <td><?php echo $row['city']; ?></td>
                        <td><?php echo $row['order_payment']; ?></td>
                        <td>
                        <button type="button" id="pendingBtn<?php echo $row['order_id']; ?>" onclick="statusOrder(<?php echo $row['order_id']; ?>, 'pending')" class="btn btn-primary btn-block">Pending</button>
                        <button type="button" id="preparingBtn<?php echo $row['order_id']; ?>" onclick="statusOrder(<?php echo $row['order_id']; ?>, 'preparing')" class="btn btn-primary btn-block">Preparing</button>
                        <button type="button" id="processBtn<?php echo $row['order_id']; ?>" onclick="statusOrder(<?php echo $row['order_id']; ?>, 'delivery on process')" class="btn btn-primary btn-block">Delivery On Process</button>
                        <button type="button" id="deliveredBtn<?php echo $row['order_id']; ?>" onclick="statusOrder(<?php echo $row['order_id']; ?>, 'delivered')" class="btn btn-primary btn-block">Delivered</button>
                        <button type="button" id="cancelBtn<?php echo $row['order_id']; ?>" onclick="statusOrder(<?php echo $row['order_id']; ?>, 'cancelled')" class="btn btn-primary btn-block">Cancel</button>
                        </td>
                        <td class="text-center">
                          <button type="button" onclick="viewOrder(<?php echo $row['order_id']; ?>)" class="btn btn-primary btn-block">View</button>
                          <br>
                          
                        </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">View Order</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Customer Info:</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Fullname</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="fullname" placeholder="Fullname" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Event Schedule</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Date</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eventDate" placeholder="Event Date" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" placeholder="Address" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Order Details</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Status*</label>
                      <div class="col-sm-10">
                      <select class="form-control" id="orderStatus" disabled="true"><!-- to do cancel order-->
                          <option value="pending">pending</option>
                          <option value="preparing">preparing</option>
                          <option value="delivery on process">delivery on process</option>
                          <option value="delivered">delivered</option>
                          <option value="cancelled">cancelled</option>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="orderType" placeholder="Order Type" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Payment</h5>
                  <hr>
                  <form>
                    <img id="referenceImage" src="../img/menu_img/not-available.png" alt=" Image Not Found!" width="200" height="200" class="img-thumbnail mx-auto d-block mb-5">
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Payment Method</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="paymentMethod" placeholder="Payment Method" disabled>
                      </div>
                    </div>
                    <div id="rpForm" class="form-group row" style="display: none">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Remittance Provider</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="remittanceProvider" placeholder="Remittance Provider" disabled>
                      </div>
                    </div>
                    <div id="rnForm" class="form-group row" style="display: none">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Reference Number</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="referenceNumber" placeholder="Reference Number" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Order Payment</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="orderPayment" placeholder="Reference Number" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Amount Paid</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control" id="amountPaid" placeholder="Amount Paid">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Downpayment</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control" id="downpayment" placeholder="Downpayment">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Remaining Balance</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="remBal" placeholder="Remaining Balance" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Payment Status</label>
                      <div class="col-sm-10">
                        <select class="form-control" id="paymentStatus">
                              <option value="paid">paid</option>
                              <option value="unpaid">unpaid</option>
                        </select>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Order Line</h5>
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Menu</th>
                      <th scope="col">Category</th>
                      <th scope="col">Size</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Price</th>
                    </tr>
                  </thead>
                  <tbody id="menuTable">
                  </tbody>
                </table>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Services</h5>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Utility</th>
                        <th scope="col">Quantity</th>
                      </tr>
                    </thead>
                    <tbody id="serviceTable">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="updateOrder()">Save changes</button>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<script>
let orderDetails = <?php echo json_encode($reservationList) ?>;
let focusOrderId = '';
let orderElement = [];

for (let index = 0; index  < orderDetails.length; index++) {
  if (orderDetails[index].order_status == "pending") {
    $(`#pendingBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "preparing") {
    $(`#preparingBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "delivery on process") {
    $(`#processBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "delivered") {
    $(`#processBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#cancelBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#preparingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#pendingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${orderDetails[index].order_id}`).addClass('btn-success').attr("disabled", "true")
  }

  if (orderDetails[index].order_status == "cancelled") {
    $(`#cancelBtn${orderDetails[index].order_id}`).addClass('btn-success').attr("disabled", "true")
    $(`#processBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#preparingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#pendingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${orderDetails[index].order_id}`).attr("disabled", "true")
  }
}

function statusOrder (order_id, status) {
  let customersOrder = orderDetails.find(function (data) {
    return data.order_id == order_id
  })

  $(`#pendingBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#preparingBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#processBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#deliveredBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#cancelBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  

  if (status == "pending") {
    $(`#pendingBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "preparing") {
    $(`#preparingBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "delivery on process") {
    $(`#processBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "delivered") {
     $(`#processBtn${order_id}`).attr("disabled", "true")
     $(`#cancelBtn${order_id}`).attr("disabled", "true")
     $(`#preparingBtn${order_id}`).attr("disabled", "true")
     $(`#pendingBtn${order_id}`).attr("disabled", "true")
     $(`#deliveredBtn${order_id}`).removeClass('btn-primary').addClass('btn-success').attr("disabled", "true")
  }

  if (status == "cancelled") {
    $(`#cancelBtn${order_id}`).removeClass('btn-primary').addClass('btn-success').attr("disabled", "true")
    $(`#processBtn${order_id}`).attr("disabled", "true")
    $(`#preparingBtn${order_id}`).attr("disabled", "true")
    $(`#pendingBtn${order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${order_id}`).attr("disabled", "true")
  }

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {
      order_id: order_id, 
      order_status: status, 
      requestType: 'updateCustomerOrderStatus'},
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Error!", data.message, "warning")
        return;
    }

    swal('Success!', 'Customer Order Updated!', 'success');

    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function viewOrder (order_id) {
  focusOrderId = order_id;
   $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {order_id: order_id, requestType: 'getOrderLineList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        
        let orderDetail = orderDetails.find(data => data.order_id == order_id)

        if (orderDetail.notif_status == 0) {
          updateStatus(order_id);
        }

        let payment = data.payment
        let menu = data.menu
        let services = data.service
        let order = data.order

        let fullname = `${orderDetail.cust_fname} ${orderDetail.cust_lname}`
        let address = `${order.address} ${order.city}`

        $('#fullname').val(fullname)
        $('#eventDate').val(order.order_date)
        $('#address').val(address)
        $('#orderStatus').val(order.order_status)
        $('#orderType').val(order.order_type)
        $('#orderPayment').val(order.order_payment)
       
        if (payment.payment_type == 'Money Remittance') {
            $('#rpForm').show()
            $('#rnForm').show()
            $('#paymentMethod').val(payment.payment_type)
            $('#remittanceProvider').val(payment.remittance_provider)
        }

        let menuTemplate = ''
        let serviceTemplate = ''

        $('#menuTable').empty()
        $('#serviceTable').empty()
     
        $('#referenceImage').attr('src', '<?php echo $_ENV['base_url_client']; ?>images/reference/'+ payment.reference_img)
        $('#referenceNumber').val(payment.reference_number)
        $('#amountPaid').val(payment.amount_paid)
        $('#downpayment').val(payment.downpayment)
        $('#paymentStatus').val(payment.payment_status)

        if (payment.downpayment == 0.00) {
            $('#downpayment').prop('disabled', false);
        } else {
          $('#downpayment').prop('disabled', true);
        }

        if (payment.amount_paid == 0.00 && parseFloat(payment.rem_bal) == 0.00) {
            $('#amountPaid').prop('disabled', true);
        } else {
            $('#amountPaid').prop('disabled', false);
        }

        if (parseFloat(payment.rem_bal) == 0.00) {
            $('#amountPaid').prop('disabled', true);
            $('#downpayment').prop('disabled', true);
        }


        if (payment.payment_status == 'paid') {
            $('#paymentStatus').prop('disabled', true)            
        }

        // let remaining_balance = Math.abs(payment.amount_paid - payment.rem_bal - payment.downpayment)

        $('#remBal').val(payment.rem_bal)
        $('#orderModal').modal('show')

        for (let index = 0; index < menu.length; index++) {
          menuTemplate = `<tr id="menuRow"><td>${menu[index].food_name}</td> <td>${menu[index].category}</td> <td>${menu[index].size}</td> <td>${menu[index].quantity}</td> <td>${menu[index].price_size}</td></td>` + menuTemplate
        }


        for (let index = 0; index < services.length; index++) {
          serviceTemplate = `<tr id="serviceRow"><td>${services[index].utility_name}</td> <td>1</td></td>` + serviceTemplate
        }

        $('#menuTable').append(menuTemplate)
        $('#serviceTable').append(serviceTemplate)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

function updateOrder () {
  let order_id = focusOrderId;
  let order_status = $('#orderStatus').val()
  let amount_paid = $('#amountPaid').val()
  let downpayment = parseFloat($('#downpayment').val())
  let remaining_balance = parseFloat($('#remBal').val());
  let payment_status = $('#paymentStatus').val()
  let orderDetail = orderDetails.find(data => data.order_id == order_id)

  if (remaining_balance != 0.00) {
      // if (parseFloat(downpayment) > parseFloat(remaining_balance)) {
      //   swal('Wait!', 'Money is greater than Remaining Balance', 'info')
      //   return;
      // }

      if (!$('#amountPaid').attr('disabled') || amount_paid != remaining_balance) {
        if (parseFloat(amount_paid) > remaining_balance) {
          swal('Wait!', 'Money is greater than Remaining Balancer', 'info')
          return;
        }

        remaining_balance = Math.abs(parseFloat(amount_paid) - remaining_balance)
      }

      if (!$('#downpayment').attr('disabled')) {
        if (parseFloat(downpayment) > parseFloat(remaining_balance)) {
            swal('Wait!', 'Money is greater than Remaining Balance', 'info')
            return;
        }

        if (downpayment > remaining_balance && remaining_balance != 0) {
          swal('Wait!', 'Money is greater than Remaining Balancer', 'info')
          return;
        }

        if (downpayment != 0.00 && remaining_balance != 0) {
            remaining_balance = Math.abs(parseFloat(remaining_balance.toFixed(2)) - downpayment)
        }
      }

      $('#remBal').val(remaining_balance.toFixed(2))
  }

  if (remaining_balance === 0 || remaining_balance === 0.00) {
        $('#amountPaid').prop('disabled', true);
        $('#downpayment').prop('disabled', true);
        $('#paymentStatus').prop('disabled', true);
        payment_status = 'paid'
        $('#paymentStatus').val(payment_status)
  }

  if (downpayment !== 0.00 || downpayment !== 0) {
     $('#downpayment').prop('disabled', true);
  }

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {
      order_id: order_id, 
      order_status: order_status, 
      amount_paid: amount_paid,
      downpayment: downpayment,
      payment_status: payment_status,
      remaining_balance: remaining_balance, 
      requestType: 'updateCustomerOrder'},
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Error!", data.message, "warning")
        return;
    }

    swal('Success!', 'Customer Order Updated!', 'success');

    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function updateStatus (order_id) {
   $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        order_id: order_id,
        requestType: 'updateOrderStatus'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
      }

      $(`#orderTableRow${order_id}`).removeClass('table-warning odd')
      $(`#orderTableRow${order_id}`).addClass('table-success odd')
    },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    }) 
} 

</script>