<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../controllers/hashController.php');   
$hash = new hashController();
?>

<?php require('../src/layouts/header.php');?>

<?php require_once('../controllers/packageController.php'); 
  $package = new packageController();
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Package List</h1>
          <a href="<?php echo $_ENV["base_url"]?>views/create-package.php" class="btn btn-primary mb-3">Create Package</a>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Category</th>
                      <th>Price</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($package->getPackageList() as $row) { ?>
                    <tr id="rowPackage<?php echo $row['package_id']; ?>">
                      <td><?php echo $row['package_name']; ?></td>
                      <td><?php echo $row['package_desc']; ?></td>
                      <td><?php echo $row['category']; ?></td>
                      <td><?php echo $row['price']; ?></td>
                      <td>
                        <a class="btn btn-primary btn-sm btn-block" href="<?php echo $_ENV["base_url"]?>views/packageView.php?package_id=<?php echo $hash->encryptHash($row['package_id']); ?>" role="button">view</a>
                        <button type="button" class="btn btn-danger btn-sm btn-block" onclick="deletePackage(<?php echo $row['package_id']; ?>)">Delete</button>
                        <button type='button' id = "available<?php echo $row['package_id']; ?>" class='btn btn-primary btn-sm btn-block' onclick='updatePackageAvailability(<?php echo $row['package_id']; ?>, 0)' <?php echo $row['is_available'] == '0' ? "style='display:none'": "display" ?>>Available</button>           
                        <button type='button' id ="temporaryUnavailable<?php echo $row['package_id']; ?>" class='btn btn-warning btn-sm btn-block' onclick='updatePackageAvailability(<?php echo $row['package_id']; ?> , 1)' <?php echo $row['is_available'] == '1' ? "style='display:none'": "display" ?>>Temporary Unavailable</button> 
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script>
  function viewPackage (id) {
    // window.location.href = ; //hash
  }

  function deletePackage (package_id) {
    swal({
    title: "Are you sure you want to delete this package?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          package_id: package_id, 
          requestType: 'deletePackage'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          $(`#rowPackage${package_id}`).remove()
          swal("Deleted!", "Package deleted!", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
  }

  function updatePackageAvailability(id,status) {
 console.log(id);
  console.log(status);
 
  swal({
    title: "Are you sure?",
    text: "You want to change the availability status",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, Update Status!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          id:id,
          status: status, 
          requestType: 'updatePackageAvailability'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }
//0-unavailable 1-available
        if (status == 0) {
            $(`#temporaryUnavailable${id}`).show()
            $(`#available${id}`).hide()

          }

          if (status == 1) {
            $(`#temporaryUnavailable${id}`).hide()
            $(`#available${id}`).show()
          }
          swal("Successfully!", "You have changed the availability status.", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
 

}
</script>