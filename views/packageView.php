<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../controllers/hashController.php');   
$hash = new hashController();

$package_id = $hash->decryptHash($_GET['package_id']);
?>

<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">View Package</h1>

          <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                  <img id="packageImage" src="../img/menu_img/not-available.png" alt=" Image Not Found!" width="200" height="200" class="img-thumbnail mx-auto d-block mb-5">
                    <form>
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Package Image</label>
                        <input type="file" class="form-control-file" id="inputPackageImage" accept="image/*">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Package Name</label>
                        <input type="text" id="packageId" hidden>
                        <input type="name" class="form-control" id="packageName" aria-describedby="emailHelp" placeholder="Package Name" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <select class="form-control" id="category">
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Price per head</label>
                        <input type="number" class="form-control" id="price" aria-describedby="emailHelp" placeholder="Price" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Guest Limit</label>
                        <input type="number" class="form-control" min="1" id="limit" aria-describedby="emailHelp" placeholder="Limit" required>
                      </div>
                       <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control" id="description" rows="3" required></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-8">
                    <h3>Package</h3>
                    <table class="table">
                      <thead>
                        <tr class="d-flex">
                          <th class='col-3'>Menu Name</th>
                          <th class='col-2'>Category</th>
                          <th class='col-2'>Size</th>
                          <!-- <th class='col-2'>Price</th> -->
                          <th class='col-3'>Quantity</th>
                        </tr>
                      </thead>
                      <tbody id="packageTableItems">
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
              <div class="card-footer">
                <button type="button" class="btn btn-success float-left" onclick="updatePackage()">Update Package</button>
                <button type="button" class="btn btn-danger float-right" onclick="deletePackage()">Delete Package</button>
                <button type="button" class="btn btn-primary float-right mr-1" onclick="menuList()">Add Menu</button>
              </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script type="text/javascript">
  let menu = ''
  let package = {}
  let packageItem = []
  let packageItemDelete = []
  let packageItemCreate = []
  let searchParams = new URLSearchParams(window.location.search)
  let package_id = "<?php echo $package_id?>";

  let size_template = {
    'extra small': 'xs',
    'small': 's',
    'medium': 'm',
    'large': 'l'
  }

  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        requestType: 'getMenuList'
      },
      dataType: 'JSON',
      success: function (data) {
        menu = data

        let menu_category = [...new Set(data.data.map(item => item.category))];
        let header_template = ''

        for (let i = 0; i < menu_category.length; i++) {

          let template = ''
          for (let index = 0; index < data.data.length; index++) {
            if (menu_category[i] == data.data[index].category) {
               template = template + ' ' + `
               <table class='table table-bordered'>
                <tbody>
                  <tr>
                    <td style='width: 50%'>${data.data[index].food_name}</td>
                    <td style='width: 50%' class='text-right'>
                      <div class='btn-group' role='group' aria-label='Basic example'>
                        <button type='button' id='btnRmv${data.data[index].food_id}xs' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}xs' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_xsmall}, 'extra small')">XS</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}s' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}s' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_small}, 'small')">S</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}m' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}m' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_medium}, 'medium')">M</button>

                        <button type='button' id='btnRmv${data.data[index].food_id}l' class='btn btn-danger' style='display:none' onclick="removePackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L <i class="fa fa-trash" aria-hidden="true"></i></button>

                        <button type='button' id='btnAdd${data.data[index].food_id}l' class='btn btn-primary' onclick="addPackageItem(${data.data[index].food_id}, ${data.data[index].price_large}, 'large')">L</button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>`
            }
          }

          header_template = `<div class='col-md-12'>
          <h4>${menu_category[i]}</h4>
          ${template}
          </div>`

          $('#modal-menu-list').append(header_template);
        }
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {categoryType: 'package', dataType: 'JSON', requestType: 'getCategoryList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Oh no!", data.message, "warning")
          return;
        }

        let template = ''
        for (let index = 0; index < data.data.length; index++) {
          template = `<option>${data.data[index].category_name}</option>` + template
        }

        $('#category').append(template);      
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })

  if (package_id) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {
        dataType: 'JSON',
        package_id: package_id,
        requestType: 'getPackage'
      },
      dataType: 'JSON',
      success: function (data) {
        console.log(data)
        package = data.package
        $('#packageName').val(data.package.package_name)
        $('#category').val(data.package.category)
        $('#price').val(data.package.price)
        $('#description').val(data.package.package_desc)
        $('#limit').val(data.package.limit)
        $('#packageId').val(data.package.package_id)
        $('#packageImage').attr('src', '<?php echo $_ENV['base_url']; ?>img/package_img/' + data.package.package_image)
        $('#packageImage').val(data.package.package_image)

        if (!data.package_item.length) {
          return;
        }

        console.log(data)

        let template = ''
        for (let index = 0; index < data.package_item.length; index++) {
          packageItem.push(data.package_item[index])
          $(`#thisItem${data.package_item[index].food_id}`).attr("checked", "checked");

          $(`#btnAdd${data.package_item[index].food_id}${size_template[data.package_item[index].size]}`).hide()
          $(`#btnRmv${data.package_item[index].food_id}${size_template[data.package_item[index].size]}`).show()

          template = `<tr id="packageItemRow${data.package_item[index].food_id}${size_template[data.package_item[index].size]}" class='d-flex'><th class='col-3'>${data.package_item[index].food_name}</th> <td class='col-2'>${data.package_item[index].category}</td> <td class='col-2'>${data.package_item[index].size}</td><td class='col-3'><div class='input-group mb-3'>
                  <div class='input-group-prepend'>
                    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${data.package_item[index].food_id}, "${data.package_item[index].size}", "-", "packageItemRow")'>-</button>
                  </div>
                  <input type='text' class='form-control' id='menuQuantity${data.package_item[index].food_id}${size_template[data.package_item[index].size]}' value='${data.package_item[index].quantity}' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
                   <div class='input-group-append'>
                    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${data.package_item[index].food_id}, "${data.package_item[index].size}", "+", "packageItemRow")'>+</button>
                  </div>
                </div></td></tr>` + template;
        }

        $('#packageTableItems').append(template);
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
  }
  })

function updatePackage () {
  // GET VALUES VIA ID
  let package_id = $('#packageId').val()
  let packageName = $('#packageName').val()
  let category = $('#category').val()
  let price = $('#price').val()
  let description = $('#description').val()
  let current_pkg_img = $('#packageImage').val();
  let limit = $('#limit').val();

  // GET FILE IMAGE VIA ID
  let file_data = $('#inputPackageImage').prop('files')[0]   
  
  // INITIALIZE NEW FORM
  let form_data = new FormData()

  // APPEND ALL VALUES TO FORM DATA
  form_data.append('package_id', package_id)
  form_data.append('packageName', packageName)
  form_data.append('category', category)
  form_data.append('price', price)
  form_data.append('limit', limit)
  form_data.append('description', description)
  form_data.append('packageItem', JSON.stringify(packageItem))
  form_data.append('packageItemDelete', JSON.stringify(packageItemDelete))
  form_data.append('packageItemCreate', JSON.stringify(packageItemCreate))
  form_data.append('current_pkg_img', current_pkg_img)
  form_data.append('requestType', 'updatePackage')                  
  
  // CHECK IF THERE IS A NEW INPUTED FILE
  if (file_data) {
    form_data.append('packageFile', file_data)
  }


  // POST DATA TO CONTROLLER
  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: form_data,
    contentType: false,
    cache: false,
    processData:false,
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      if (file_data) {
        $('#packageImage').attr('src', "<?php echo $_ENV['base_url']; ?>img/package_img/" + file_data.name)
      }

      swal("Yeheey!", 'Package Updated!', "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function addPackageItem (id, price, size) {
  let template = ''
  let quantity = 1
  let packageItemFood = {}

  let package_template = {
    food_id: '',
    price_size: '',
    quantity: ''
  }

  let food =  menu.data.find(function (data) {
    return data.food_id == id
  })

  let exist = false

  for (let index = 0; index < packageItem.length; index ++) {
    if (parseInt(packageItem[index].food_id) === food.food_id && packageItem[index].size === size) {
      packageItemFood = packageItem[index]
      exist = true
      break;
    }
  }

  if (!exist) {
    package_template.food_id = food.food_id;
    package_template.quantity = quantity;
    package_template.size = size;
    package_template.price_size = price;
    packageItemCreate.push(package_template)

    template = `<tr id="packageItemRow${food.food_id}${size_template[size]}" class='d-flex'><th class='col-3'>${food.food_name}</th> <td class='col-2'>${food.category}</td> <td class='col-2'>${size}</td> <td class='col-3'><div class='input-group mb-3'>
    <div class='input-group-prepend'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "-", "packageItemCreate")'>-</button>
    </div>
    <input type='text' class='form-control' id='menuQuantity${food.food_id}${size_template[size]}' value='1' min='1' max='100' aria-label='' aria-describedby='basic-addon1' readonly>
    <div class='input-group-append'>
    <button class='btn btn-outline-secondary' type='button' onclick='quantityModifier(${food.food_id}, "${package_template.size}", "+", "packageItemCreate")'>+</button>
    </div>
    </div></td></tr>`;
    $(`#btnRmv${id}${size_template[size]}`).show()
    $(`#btnAdd${id}${size_template[size]}`).hide()
    $('#packageTableItems').append(template);
  }
}

function removePackageItem (id, price, size) {
  let indexItem = null
  let packageItemFood = {}

  for (let index = 0; index < packageItem.length; index ++) {
    if (parseInt(packageItem[index].food_id) === id && packageItem[index].size === size) {
      indexItem = index
      packageItemFood = packageItem[index]
      break;
    }
  }

  if (indexItem || indexItem == 0) {
    $(`#btnRmv${id}${size_template[size]}`).hide()
    $(`#btnAdd${id}${size_template[size]}`).show()
    $(`#packageItemRow${id}${size_template[size]}`).remove()
    packageItemDelete.push(packageItemFood)
    packageItem.splice(indexItem, 1);
  }
}

function quantityModifier (id, size, operator, packageType) {
  let quantity = parseInt($(`#menuQuantity${id}${size_template[size]}`).val());
  let indexItem = null

  if (packageType == 'packageItemCreate') {
    for (let index = 0; index < packageItemCreate.length; index ++) {
      if (parseInt(packageItemCreate[index].food_id) === id && packageItemCreate[index].size === size) {
          indexItem = index
          break;
      }
    }
  } else {
    for (let index = 0; index < packageItem.length; index ++) {
      if (parseInt(packageItem[index].food_id) === id && packageItem[index].size === size) {
          indexItem = index
          break;
      }
    }
  }

  if (indexItem || indexItem === 0) {
    if (operator === '+') {
      quantity = quantity + 1
      packageType == 'packageItemCreate' ? packageItemCreate[indexItem].quantity = quantity : packageItem[indexItem].quantity = quantity
      $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
    }
  
    if (operator === '-') {
      if(quantity > 1) {
        quantity = quantity - 1
        packageType == 'packageItemCreate' ? packageItemCreate[indexItem].quantity = quantity : packageItem[indexItem].quantity = quantity
        $(`#menuQuantity${id}${size_template[size]}`).val(quantity);
      }
    }
  }

  }

  
  function deletePackage () {
    let package_id = $('#packageId').val();
    
    swal({
    title: "Are you sure?",
    text: "Your will not be able to recover this imaginary file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          package_id: package_id, 
          requestType: 'deletePackage'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          swal("Deleted!", "Your imaginary file has been deleted.", "success");
          window.location.href = `<?php echo $_ENV["base_url"]?>views/package.php`;      
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
  }
</script>

