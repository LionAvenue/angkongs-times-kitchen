<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/reservationController.php');
  $reservation = new reservationController();
  $reservationList = array();

  if (isset($_POST['orderType'])) {
     $reservationList = $reservation->getOrderListByAll($_POST['orderType']);
  } else {
     $reservationList = $reservation->getOrderListByAll();
  }
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Payment Logs</h1>

         <form class="form-inline" action="payment.php" method="post">
          <div class="form-group mb-2">
                <select class="form-control" name="orderType" id="categoryInput">
                  <option value="pending">pending</option>
                  <option value="preparing">preparing</option>
                  <option value="delivery on process">delivery on process</option>
                  <option value="delivered">delivered</option>
                  <option value="cancelled">cancelled</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success ml-2 mb-2">Change Filter</button>
          </form>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Payment ID</th>
                      <th>Order ID</th>
                      <th>TK No.</th>
                      <th>Customer Name</th>
                      <th>Order Type</th>
                      <th>Order Status</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>Payment</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($reservationList as $row) { ?>
                    <tr id="orderTableRow<?php echo $row['order_id']?>">
                        <td><?php echo $row['payment_id']; ?></td>
                        <td><?php echo $row['order_id']; ?></td>
                        <td><?php echo $row['order_tracking_no']; ?></td>
                        <td><?php echo $row['cust_fname'] .' '. $row['cust_lname']; ?></td>
                        <td><?php echo $row['order_type']; ?></td>
                        <td><?php echo $row['order_status']; ?></td>
                        <td><?php echo $row['address']; ?></td>
                        <td><?php echo $row['city']; ?></td>
                        <td><?php echo $row['order_payment']; ?></td>
                        <td class="text-center">
                          <button type="button" onclick="viewOrder(<?php echo $row['order_id']; ?>)" class="btn btn-primary btn-block">View</button>
                          <br>
                          
                        </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">View Order</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Customer Info:</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Fullname</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="fullname" placeholder="Fullname" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Event Schedule</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Date</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eventDate" placeholder="Event Date" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" placeholder="Address" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Order Details</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Status*</label>
                      <div class="col-sm-10">
                      <select class="form-control" id="orderStatus" disabled="true"><!-- to do cancel order-->
                          <option value="pending">pending</option>
                          <option value="preparing">preparing</option>
                          <option value="delivery on process">delivery on process</option>
                          <option value="delivered">delivered</option>
                          <option value="cancelled">cancelled</option>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="orderType" placeholder="Order Type" disabled>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Payment</h5>
                  <hr>
                  <form>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Payment Method</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="paymentMethod" placeholder="Payment Method" disabled>
                      </div>
                    </div>
                    <div id="rpForm" class="form-group row" style="display: none">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Remittance Provider</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="remittanceProvider" placeholder="Remittance Provider" disabled>
                      </div>
                    </div>
                    <div id="rnForm" class="form-group row" style="display: none">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Reference Number</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="referenceNumber" placeholder="Reference Number" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Order Payment</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="orderPayment" placeholder="Reference Number" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Amount Paid</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control" id="amountPaid" placeholder="Amount Paid" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Downpayment</label>
                      <div class="col-sm-10">
                        <input type="number" class="form-control" id="downpayment" placeholder="Downpayment" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Remaining Balance</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="remBal" placeholder="Remaining Balance" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Payment Status</label>
                      <div class="col-sm-10">
                        <select class="form-control" id="paymentStatus" readonly>
                              <option value="paid">paid</option>
                              <option value="unpaid">unpaid</option>
                        </select>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Order Line</h5>
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Menu</th>
                      <th scope="col">Category</th>
                      <th scope="col">Size</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Price</th>
                    </tr>
                  </thead>
                  <tbody id="menuTable">
                  </tbody>
                </table>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-body">
                  <h5 class="card-title">Services</h5>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Utility</th>
                        <th scope="col">Quantity</th>
                      </tr>
                    </thead>
                    <tbody id="serviceTable">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<script>
let orderDetails = <?php echo json_encode($reservationList) ?>;
let focusOrderId = '';
let orderElement = [];

for (let index = 0; index  < orderDetails.length; index++) {
  if (orderDetails[index].order_status == "pending") {
    $(`#pendingBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "preparing") {
    $(`#preparingBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "delivery on process") {
    $(`#processBtn${orderDetails[index].order_id}`).addClass('btn-success')
  }

  if (orderDetails[index].order_status == "delivered") {
    $(`#processBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#cancelBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#preparingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#pendingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${orderDetails[index].order_id}`).addClass('btn-success').attr("disabled", "true")
  }

  if (orderDetails[index].order_status == "cancelled") {
    $(`#cancelBtn${orderDetails[index].order_id}`).addClass('btn-success').attr("disabled", "true")
    $(`#processBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#preparingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#pendingBtn${orderDetails[index].order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${orderDetails[index].order_id}`).attr("disabled", "true")
  }
}

function statusOrder (order_id, status) {
  let customersOrder = orderDetails.find(function (data) {
    return data.order_id == order_id
  })

  $(`#pendingBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#preparingBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#processBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#deliveredBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  $(`#cancelBtn${order_id}`).removeClass('btn-success').addClass('btn-primary')
  

  if (status == "pending") {
    $(`#pendingBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "preparing") {
    $(`#preparingBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "delivery on process") {
    $(`#processBtn${order_id}`).removeClass('btn-primary').addClass('btn-success')
  }

  if (status == "delivered") {
     $(`#processBtn${order_id}`).attr("disabled", "true")
     $(`#cancelBtn${order_id}`).attr("disabled", "true")
     $(`#preparingBtn${order_id}`).attr("disabled", "true")
     $(`#pendingBtn${order_id}`).attr("disabled", "true")
     $(`#deliveredBtn${order_id}`).removeClass('btn-primary').addClass('btn-success').attr("disabled", "true")
  }

  if (status == "cancelled") {
    $(`#cancelBtn${order_id}`).removeClass('btn-primary').addClass('btn-success').attr("disabled", "true")
    $(`#processBtn${order_id}`).attr("disabled", "true")
    $(`#preparingBtn${order_id}`).attr("disabled", "true")
    $(`#pendingBtn${order_id}`).attr("disabled", "true")
    $(`#deliveredBtn${order_id}`).attr("disabled", "true")
  }

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {
      order_id: order_id, 
      order_status: status, 
      requestType: 'updateCustomerOrderStatus'},
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Error!", data.message, "warning")
        return;
    }

    swal('Success!', 'Customer Order Updated!', 'success');

    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function viewOrder (order_id) {
  focusOrderId = order_id;
   $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {order_id: order_id, requestType: 'getOrderLineList'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        
        let orderDetail = orderDetails.find(data => data.order_id == order_id)

        if (orderDetail.notif_status == 0) {
          updateStatus(order_id);
        }

        let payment = data.payment
        let menu = data.menu
        let services = data.service
        let order = data.order

        let fullname = `${orderDetail.cust_fname} ${orderDetail.cust_lname}`
        let address = `${order.address} ${order.city}`

        $('#fullname').val(fullname)
        $('#eventDate').val(order.order_date)
        $('#address').val(address)
        $('#orderStatus').val(order.order_status)
        $('#orderType').val(order.order_type)
        $('#orderPayment').val(order.order_payment)
       
        if (payment.payment_type == 'Money Remittance') {
            $('#rpForm').show()
            $('#rnForm').show()
            $('#paymentMethod').val(payment.payment_type)
            $('#remittanceProvider').val(payment.remittance_provider)
        }

        let menuTemplate = ''
        let serviceTemplate = ''

        $('#menuTable').empty()
        $('#serviceTable').empty()
     
        $('#referenceNumber').val(payment.reference_number)
        $('#amountPaid').val(payment.amount_paid)
        $('#downpayment').val(payment.downpayment)

        $('#paymentStatus').val(payment.payment_status)

        // let remaining_balance = Math.abs(payment.amount_paid - payment.rem_bal - payment.downpayment)

        $('#remBal').val(payment.rem_bal)
        $('#orderModal').modal('show')

        for (let index = 0; index < menu.length; index++) {
          menuTemplate = `<tr id="menuRow"><td>${menu[index].food_name}</td> <td>${menu[index].category}</td> <td>${menu[index].size}</td> <td>${menu[index].quantity}</td> <td>${menu[index].price_size}</td></td>` + menuTemplate
        }


        for (let index = 0; index < services.length; index++) {
          serviceTemplate = `<tr id="serviceRow"><td>${services[index].utility_name}</td> <td>1</td></td>` + serviceTemplate
        }

        $('#menuTable').append(menuTemplate)
        $('#serviceTable').append(serviceTemplate)
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}

</script>