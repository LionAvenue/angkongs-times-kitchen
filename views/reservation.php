<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Reservation</h1>

          <div id='calendar'></div>
        </div>
        <!-- /.container-fluid -->

      </div>

<style>
  .fc-content {
    color: #fff;
  }
</style>

<?php require('../src/layouts/footer.php');?>

<script>

let orderDates = [];

$( document ).ready(function() {
  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: {requestType: 'getOrderList'},
    dataType: 'JSON',
    success: function (data) {
      let available_dates = data.data

      for (let index = 0; index < available_dates.length; index++) {
        orderDates.push({'title': 'Customer Order', 'start': available_dates[index].created_at})
      }

      if (orderDates.length) {
        renderCalendar(orderDates)
      }
    },
    error: function (data) {
      swal("Oh no!", "You clicked the button!", "warning")
    }
  })
})

function renderCalendar (orderDates) {
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    defaultDate: moment().format('Y-MM-DD'),
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: orderDates,
    eventClick: function(calEvent, jsEvent, view) {
        let date = calEvent.start.format('Y-MM-DD')
        window.location = `<?php echo $_ENV["base_url"]?>views/order.php?date=${date}`;
      }
  });
}

      // {
      //   id: 999,
      //   title: 'Repeating Event',
      //   start: '2019-01-09T16:00:00'
      // },
      // {
      //   id: 999,
      //   title: 'Repeating Event',
      //   start: '2019-01-16T16:00:00'
      // },
      // {
      //   title: 'Conference',
      //   start: '2019-01-11',
      //   end: '2019-01-13'
      // },
      // {
      //   title: 'Meeting',
      //   start: '2019-01-12T10:30:00',
      //   end: '2019-01-12T12:30:00'
      // },
      // {
      //   title: 'Lunch',
      //   start: '2019-01-12T12:00:00'
      // },
      // {
      //   title: 'Meeting',
      //   start: '2019-01-12T14:30:00'
      // },
      // {
      //   title: 'Happy Hour',
      //   start: '2019-01-12T17:30:00'
      // },
      // {
      //   title: 'Dinner',
      //   start: '2019-01-12T20:00:00'
      // },
      // {
      //   title: 'Birthday Party',
      //   start: '2019-01-13T07:00:00'
      // },
      // {
      //   title: 'Click for Google',
      //   url: 'http://google.com/',
      //   start: '2019-01-28'
      // }


</script>

