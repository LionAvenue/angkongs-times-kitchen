<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <h1>You Are Restricted For This Page</h1>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>