<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/servicesController.php'); 
  $service = new servicesController();
  $serviceList = $service->getServiceList();
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Utility Name</th>
                      <th>Reserved By</th>
                      <th>Total</th>
                      <th>Status</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                      <?php foreach ($serviceList as $row) { ?>
                          <tr>  
                              <td><?php echo $row['utility_name']; ?></td>
                              <td><?php echo $row['cust_fname'].' '. $row['cust_lname']; ?></td>
                              <td><?php echo $row['quantity']; ?></td>
                              <td id="serviceStatus<?php echo $row['service_id']; ?>"><?php echo $row['status']; ?></td>
                              <td class="text-center">
                                <?php if ($row['status'] == 'unreturned') { ?>
                                  <button id="serviceRtnBtn<?php echo $row['service_id']; ?>" class="btn btn-primary btn-block" onclick="returnItem(<?php echo $row['service_id']; ?>, <?php echo $row['utility_id']; ?>, <?php echo $row['quantity']; ?>)">Return Item</button>
                                <?php } ?>
                              </td>
                          </tr>
                     <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script>
function returnItem (service_id, utility_id, quantity) {
    $.ajax({
      type: 'POST',
      url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
      data: {service_id: service_id, utility_id: utility_id, quantity: quantity, status: 'returned', requestType: 'returnUtility'},
      dataType: 'JSON',
      success: function (data) {
        if (data.status != 'OK') {
          swal("Error!", data.message, "warning")
          return;
        }
        $(`#serviceRtnBtn${service_id}`).hide()
        $(`#serviceStatus${service_id}`).text('returned')
        swal('Yeah!', 'Utility Returned!', 'success')       
      },
      error: function (data) {
        swal("Oh no!", 'Server Error', "warning")
      }
    })
}
</script>