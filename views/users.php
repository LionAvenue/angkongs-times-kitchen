<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/userController.php'); 
  $users = new userController();
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Users</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Firstname</th>
                      <th>Lastname</th>
                      <th>Middle Name</th>
                      <th>Address</th>
                      <th>Mobile No.</th>
                      <th>Email</th>
                      <th>Username</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($users->getUserList() as $row) { ?>
                    <tr>
                      <td><?php echo $row['cust_fname']; ?></td>
                      <td><?php echo $row['cust_lname']; ?></td>
                      <td><?php echo $row['cust_mi']; ?></td>
                      <td><?php echo $row['user_add']; ?></td>
                      <td><?php echo $row['cust_cn']; ?></td>
                      <td><?php echo $row['cust_email']; ?></td>
                      <td><?php echo $row['username']; ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<?php require('../src/layouts/footer.php');?>

<script>
</script>