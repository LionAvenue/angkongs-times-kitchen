<?php require('../session/sessionController.php');
$session = new sessionController();
?>
<?php require('../restrictions/pageRestrictions.php') ?>
<?php require('../src/layouts/header.php');?>
<?php require_once('../controllers/utilityController.php'); 
  $utilityList = new utilityController();
  $utilityListObject = json_encode($utilityList->getUtilityList());
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Utility List</h1>
          <a href="<?php echo $_ENV["base_url"]?>views/create-utilities.php" class="btn btn-primary mb-3">Create Utility</a>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Utility Name</th>
                      <th>Total</th>
                      <th>Price</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody id="menuTableList">
                  <?php foreach ($utilityList->getUtilityList() as $row) { ?>
                  <tr id="rowUtility<?php echo $row['utility_id']; ?>">
                     <td id="rowUtilityName<?php echo $row['utility_id']; ?>"><?php echo $row['utility_name']; ?></td>
                     <td id="rowUtilityTotal<?php echo $row['utility_id']; ?>"><?php echo $row['utility_total']; ?></td>
                     <td id="rowUtilityPrice<?php echo $row['utility_id']; ?>"><?php echo $row['utility_price']; ?></td>
                     <td>
                     <button type='button' class='btn btn-primary btn-sm btn-block' onclick='viewUtility(<?php echo $row['utility_id']; ?>)'>View</button> 
                     <button type='button' class='btn btn-danger btn-sm btn-block' onclick='deleteUtility(<?php echo $row['utility_id']; ?>)'>Delete</button> 
                     <button type='button' id = "available<?php echo $row['utility_id']; ?>" class='btn btn-primary btn-sm btn-block' onclick='updateUtilityAvailability(<?php echo $row['utility_id']; ?>, 0)' <?php echo $row['is_available'] == '1' ? "style='display:none'": "display" ?>>Available</button>           
                     <button type='button' id ="temporaryUnavailable<?php echo $row['utility_id']; ?>" class='btn btn-warning btn-sm btn-block' onclick='updateUtilityAvailability(<?php echo $row['utility_id']; ?> , 1)' <?php echo $row['is_available'] == '0' ? "style='display:none'": "display" ?>>Temporary Unavailable</button> 
                     </td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div class="modal fade" id="viewUtilityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">View Utility</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <input type="hidden" id="inputUtilityId">
                <div class="form-group">
                  <label for="exampleInputEmail1">Utility Name</label>
                  <input type="name" class="form-control" id="inputUtilityName" aria-describedby="emailHelp" placeholder="Menu Name" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Quantity</label>
                  <input type="number" class="form-control" id="inputUtilityTotal" min="1" aria-describedby="emailHelp" placeholder="Quantity" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="number" class="form-control" id="inputUtilityPrice" min="1" aria-describedby="emailHelp" placeholder="Price" required>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="updateUtility()">Save changes</button>
            </div>
          </div>
        </div>
      </div>

<?php require('../src/layouts/footer.php');?>

<script>
let utilityListObject = <?php echo $utilityListObject; ?>;

function updateUtility () {
  // GET VALUES VIA #ID
  let utility_id = $('#inputUtilityId').val()
  let utility_name = $('#inputUtilityName').val()
  let utility_total = $('#inputUtilityTotal').val()
  let utility_price = $('#inputUtilityPrice').val()

  // CREATING NEW FORM
  let form_data = new FormData()

  // APPEND VALUES IN FORM
  form_data.append('utility_id', utility_id)
  form_data.append('utility_name', utility_name)
  form_data.append('utility_total', utility_total)
  form_data.append('utility_price', utility_price)               
  form_data.append('requestType', 'updateUtility')

  $.ajax({
    type: 'POST',
    url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
    data: form_data,
    contentType: false,
    cache: false,
    processData:false,
    dataType: 'JSON',
    success: function (data) {
      if (data.status != 'OK') {
        swal("Oh no!", data.message, "warning")
        return;
      }

      $(`#rowUtilityName${utility_id}`).text(utility_name)
      $(`#rowUtilityTotal${utility_id}`).text(utility_total)
      $(`#rowUtilityPrice${utility_id}`).text(utility_price)

      swal("Yes!", data.message, "success")       
    },
    error: function (data) {
      swal("Oh no!", 'Server Error', "warning")
    }
  })
}

function viewUtility (uty_id) {
  let utility = utilityListObject.find(function (utility) {
    return utility.utility_id == uty_id
  })

  $('#inputUtilityId').val(utility.utility_id)
  $('#inputUtilityName').val(utility.utility_name)
  $('#inputUtilityTotal').val(utility.utility_total)
  $('#inputUtilityPrice').val(utility.utility_price)
  $('#utilityImage').attr('src', `<?php echo $_ENV['base_url']; ?>img/utility_img/${utility.utility_image}`)
  $('#utilityImage').val(utility.utility_image)
  $('#viewUtilityModal').modal('show')
}

function deleteUtility (utility_id) {
    swal({
    title: "Are you sure you want to delete this?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          utility_id: utility_id, 
          requestType: 'deleteUtility'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }

          $(`#rowUtility${utility_id}`).remove()
          swal("Deleted!", "Utility deleted!", "success");       
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
}

  function updateUtilityAvailability(id,status) {
 console.log(id);
  console.log(status);
 
  swal({
    title: "Are you sure?",
    text: "You want to change the availability status",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, Update Status!",
    closeOnConfirm: false
  },
  function(isConfirm){
    if (isConfirm) {
      $.ajax({
        type: 'POST',
        url: '<?php echo $_ENV["base_url"]?>controllers/controller.php',
        data: {
          id:id,
          status: status, 
          requestType: 'updateUtilityAvailability'
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.status != 'OK') {
            swal("Oh no!", data.message, "warning")
            return;
          }
//0-unavailable 1-available
        if (status == 0) {
            $(`#temporaryUnavailable${id}`).show()
            $(`#available${id}`).hide()

          }

          if (status == 1) {
            $(`#temporaryUnavailable${id}`).hide()
            $(`#available${id}`).show()
          }
           swal("Successfully!", "You have changed the availability status.", "success");         
        },
        error: function (data) {
          swal("Oh no!", 'Server Error', "warning")
        }
      })
    }
  });
 

}
</script>